{
  /*
  Proprietary software available to you. Influences the activation of those programs in software,
  which depends on them.
  */
  gaussian = false; # Requires binary archive.
  mrcc = false;
  orca = false;
  turbomole = false;
  cfour = false;
  gamess-us = false;
  molpro = {
    enable = false;
    token = "/user/seeber/.molpro/token"; # Absolute path to the Molpro license token.

    # The network interface that MPI is supposed to use. Use "lo" (loopback) for single node
    # parallelism and your preffered high speed network interface like "ib0" on clusters.
    # See "ip addr show" for available network interfaces.
    interface = "lo";
  };

  # CPU extensions
  sse = true;
  sse2 = true;
  sse3 = true;
  sse4 = true;
  sse4_2 = true;
  avx = true;
  avx2 = true;
  avx512 = false;
  fma = true;
  aes = true;

  # GPU
  cuda = false;
  computeCapability = "6.1";

  # Compiler settings
  defaultOptimisationFlag = "-O3";

  # Cluster network
  network = "ethernet"; # One of [ "omnipath" "infiniband" "ethernet" ]

  # NixGL configuration
  useNixGL = false; # Set to false if you are on NixOS
  nixGL = null; # Full autodetection.
  /*
  nixGL = {
    type = "nvidia"; # One of [ "nvidia" "bumblebee" "intel" "mesa" ]
    nvidiaVersion = null # Version of the nvidia driver kernel module of your system, e.g. "418.133". null enables autodetection.
  };
  */

  # Select the default MPI version, where you have the choice.
  mpi = "mvapich2"; # One of [ "openmpi" "mvapich2" ]

  # Select default BLAS, where you have the choice.
  blas = "mkl"; # One of [ "mkl" "openblas" ]
}
