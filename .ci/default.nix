let
  chemix = import ../default.nix;
  pkgs = import <nixos> { overlays = [ chemix ]; };

in
  pkgs
