self: super:
let
  # Import the configuration settings from the config file
  config = import ./config.nix;

  # Performance flags (CPU extensions) available to your machine.
  performanceFlags = with super.stdenv.lib.lists; builtins.toString ([
      config.defaultOptimisationFlag
    ]
    ++ optional config.sse "-msse"
    ++ optional config.sse2 "-msse2"
    ++ optional config.sse3 "-msse3"
    ++ optional config.sse4 "-msse4"
    ++ optional config.sse4_2 "-msse4.2"
    ++ optional config.avx "-mavx"
    ++ optional config.avx2 "-mavx2"
    ++ optional config.avx512 "-mavx512f"
    ++ optional config.aes "-maes"
    ++ optional config.fma "-mfma"
  );

  # Stdenv adapter function. Takes an old stdenv and adds performance optimisations flags to it.
  purePerformanceOptimizations = stdenv: stdenv //
    { mkDerivation = args: stdenv.mkDerivation (args // {
        NIX_CFLAGS_COMPILE = toString (args.NIX_CFLAGS_COMPILE or "") + performanceFlags;
        NIX_ENFORCE_NO_NATIVE = true;

        preBuild = ''
          makeFlagsArray+=(
            FFLAGS="${performanceFlags}"
            CFLAGS="${performanceFlags}"
            CXXFLAGS="${performanceFlags}"
          );
        '';

        preferLocalBuild = false;
        allowSubstitutes = true;
      });
    };

  # Create some short aliases for functions.
  stdenvPerf = purePerformanceOptimizations super.stdenv;
in
  {
    # BLAS and MPI settings.
    # Capital MPI to not call it "mpi". This makes namespace problems and leads to infinite recursions.
    MPI = (
      assert super.stdenv.lib.asserts.assertMsg (builtins.elem config.mpi [ "openmpi" "mvapich2" ]) "Configuration of MPI must be one of [ \"mvapich2\" \"openmpi\" ].";
      if      config.mpi == "openmpi" then self.openmpi
      else if config.mpi == "mvapich2" then self.mvapich2
      else null
    );

    # OK
    blas = (
      assert super.stdenv.lib.asserts.assertMsg (builtins.elem config.blas [ "openblas" "mkl" ]) "Configuration of BLAS must be one of [ \"openblas\" \"mkl\" ].";
      if      config.blas == "openblas" then super.blas.override { blasProvider = super.openblasCompat; }
      else if config.blas == "mkl" then super.blas.override { blasProvider = super.mkl; }
      else null
    );
    # OK
    # In case MKL is chosen "isILP64" will not say "true", as it provides both 32 and 64 bit.
    blasILP = (
      assert super.stdenv.lib.asserts.assertMsg (builtins.elem config.blas [ "openblas" "mkl" ]) "Configuration of BLAS must be one of [ \"openblas\" \"mkl\" ].";
      if      config.blas == "openblas" then super.blas.override { blasProvider = super.openblas; }
      else if config.blas == "mkl" then super.blas.override { blasProvider = super.mkl; }
      else null
    );

    # OK
    lapack = (
      assert super.stdenv.lib.asserts.assertMsg (builtins.elem config.blas [ "openblas" "mkl" ]) "Configuration of BLAS/LAPACK must be one of [ \"openblas\" \"mkl\" ].";
      if      config.blas == "openblas" then super.lapack.override { lapackProvider = super.openblasCompat; }
      else if config.blas == "mkl" then super.lapack.override { lapackProvider = super.mkl; }
      else null
    );

    # Fortran and C applications
    # OK
    gdma = self.callPackage ./pkgs/apps/gdma/default.nix {
      stdenv = stdenvPerf;
    };

    # OK
    packmol = self.callPackage ./pkgs/apps/packmol/default.nix {
      stdenv = stdenvPerf;
    };

    # OK
    pysisyphus = self.python3Packages.toPythonApplication self.python3Packages.pysisyphus;
    pysisyphusUnstable = self.python3Packages.toPythonApplication self.python3Packages.pysisyphusUnstable;

    # OK
    psi4 = self.python3Packages.toPythonApplication self.python3Packages.psi4;
    psi4Unstable = self.python3Packages.toPythonApplication self.python3Packages.psi4Unstable;

    # OK
    i-pi = self.python3Packages.toPythonApplication self.python3Packages.i-pi;

    # OK
    dftd3 = self.callPackage ./pkgs/apps/dft-d3/default.nix {
      stdenv = stdenvPerf;
    };

    # OK
    orca = self.callPackage ./pkgs/apps/orca/default.nix {
      openmpi = self.openmpi;
    };

    # OK
    xtb = self.callPackage ./pkgs/apps/xtb/default.nix {
      stdenv = stdenvPerf;
      turbomole = if config.turbomole then self.turbomole else null;
      orca = if config.turbomole then self.orca else null;
    };

    # OK
    molden = self.callPackage ./pkgs/apps/molden/default.nix {};

    # OK
    gaussian = self.callPackage ./pkgs/apps/gaussian/default.nix {
      inherit config;
    };

    # OK
    mrcc = self.callPackage ./pkgs/apps/mrcc/default.nix {};

    # OK
    vmd = self.callPackage ./pkgs/apps/vmd/default.nix {
      useBin = self.stdenv.isLinux && config.cuda && self.stdenv.isx86_64;
      cudatoolkit = self.cudaPackages.cudatoolkit_8;
      config = config;
    };

    # OK
    openmolcas = self.callPackage ./pkgs/apps/openmolcas/default.nix {
      mpi = self.MPI;
    };

    # OK
    gamess-us = self.callPackage ./pkgs/apps/gamess-us/default.nix {
      stdenv = stdenvPerf;
    };

    # OK
    turbomole = self.callPackage ./pkgs/apps/turbomole/default.nix {
      useMPI = false;
    };

    # OK
    nwchem = self.callPackage ./pkgs/apps/nwchem/default.nix {
      stdenv = stdenvPerf;
      mpi = self.MPI;
    };

    # OK
    multiwfn = self.callPackage ./pkgs/apps/multiwfn/default.nix {
      nixGL = if config.nixGL != null then self.nixGL else null;
    };

    # OK
    gpaw = self.python3Packages.toPythonApplication self.python3Packages.gpaw;

    # OK
    octopus = self.callPackage ./pkgs/apps/octopus/default.nix {
      inherit config;
      stdenv = stdenvPerf;
      mpi = self.MPI;
      parallel = true;
    };

    libint = self.callPackage ./pkgs/libs/libint/default.nix {
      inherit config;
      stdenv = stdenvPerf;
    };

    # OK
    xcfun = self.callPackage ./pkgs/libs/xcfun/default.nix {
      stdenv = stdenvPerf;
    };

    # OK
    cp2k = self.callPackage ./pkgs/apps/cp2k/default.nix {
      stdenv = stdenvPerf;
      mpi = self.MPI;
    };

    # OK
    dalton = self.callPackage ./pkgs/apps/dalton/default.nix {
      stdenv = stdenvPerf;
      mpi = self.MPI;
    };

    # OK
    block = self.callPackage ./pkgs/apps/block/default.nix {
      stdenv = stdenvPerf;
      boost = self.boost155;
    };

    # OK
    molpro = self.callPackage ./pkgs/apps/molpro/default.nix {
      inherit config;
    };

    # OK
    crest = self.callPackage ./pkgs/apps/crest/default.nix {};

    # WiP
    bagel = self.callPackage ./pkgs/apps/bagel/default.nix {
      inherit config;
      stdenv = stdenvPerf;
    };

    # OK
    wfoverlap = self.callPackage ./pkgs/apps/wfoverlap/default.nix {};

    # OK
    enso = self.python3Packages.toPythonApplication self.python3Packages.enso;

    # OK
    cefine = self.callPackage ./pkgs/apps/cefine/default.nix {
      turbomole = if config.turbomole then self.turbomole else null;
    };

    # OK
    stda = self.callPackage ./pkgs/apps/stda/default.nix { };

    # OK
    xtb4stda = self.callPackage ./pkgs/apps/xtb4stda/default.nix {};

    # OK
    travis = self.callPackage ./pkgs/apps/travis/default.nix {};

    # OK
    tinker = self.callPackage ./pkgs/apps/tinker/default.nix {};

    # OK
    lichem = self.callPackage ./pkgs/apps/lichem/default.nix {
      gaussian = if config.gaussian then self.gaussian else null;
    };

    # OK
    sharc = self.callPackage ./pkgs/apps/sharc/default.nix {
      orca = if config.orca then self.orca else null;
      gaussian = if config.gaussian then self.gaussian else null;
      turbomole = if config.turbomole then self.turbomole else null;
    };

    # OK
    cfour = self.callPackage ./pkgs/apps/cfour/default.nix {};

    # WiP
    # iboview = self.callPackage ./pkgs/apps/iboview/default.nix {};

    # OK
    taskspooler = self.callPackage ./pkgs/apps/taskspooler/default.nix {};

    # Libraries
    # OK
    libxc = self.callPackage ./pkgs/libs/libxc/default.nix {
      stdenv = stdenvPerf;
    };
    libxc-4_3_3 = self.libxc.overrideAttrs (oldAttrs: rec {
      version = "4.3.3";
      src = self.fetchFromGitLab {
        owner = "libxc";
        repo = oldAttrs.pname;
        rev = version;
        sha256= "1kbnxc85jy64byk46s3dff2cxifp8yjzrkzw782zn5cs15ifl1fg";
      };
      /*
      Fixes an error when trying to link against libxc, which is cause by a simple missing file in
      CMakeList.txt, see: https://gitlab.com/libxc/libxc/-/merge_requests/165/diffs?commit_id=3f4bad352779a803020de89e271a175a72b03f85
      Fixed in 5.* .
      */
      patches = [
        (self.fetchpatch {
          name = "fix_bib_linking_error";
          url = "https://gitlab.com/libxc/libxc/-/commit/3f4bad352779a803020de89e271a175a72b03f85.diff";
          sha256 = "1k78zhnyxnfqnzjraf33fdcnhr49hrikllbg90397g9h4jfilbfw";
        })
      ];
    });

    # OK
    libint-v1 = self.callPackage ./pkgs/libs/libint/1.nix {
      stdenv = stdenvPerf;
    };

    # OK
    pcmsolver = self.callPackage ./pkgs/libs/pcmsolver/default.nix {
      stdenv = stdenvPerf;
    };

    # OK
    dkh = self.callPackage ./pkgs/libs/dkh/default.nix {
      stdenv = stdenvPerf;
    };

    # OK
    libefp = self.callPackage ./pkgs/libs/libefp/default.nix {
      stdenv = stdenvPerf;
    };
    # Special version for Python linking, cmake patches and Psi4.
    libefp_p = self.libefp.overrideAttrs (oldAttrs: rec {
      version = "1.5.0p";
      src = self.fetchFromGitHub  {
        owner = "ilyak";
        repo = oldAttrs.pname;
        rev = "15cd7ce91239c04b5c32ed101bde6cc36c57550a";
        sha256= "0jcvl3chni4f0hddx9blaia3kccfqx7cszrwavp0a35d42n0x5i2";
      };
      specialCmakeFlags = [
        "-DCMAKE_INSTALL_PREFIX=$out"
        "-DNAMESPACE_INSTALL_INCLUDEDIR=/"
        "-DCMAKE_FIND_USE_SYSTEM_PACKAGE_REGISTRY=OFF"
        "-DCMAKE_FIND_USE_PACKAGE_REGISTRY=OFF"
        "-DCMAKE_EXPORT_NO_PACKAGE_REGISTRY=ON"
        "-DCMAKE_SKIP_BUILD_RPATH=ON"
        "-DFRAGLIB_DEEP=OFF"
        "-DFRAGLIB_UNDERSCORE_L=OFF"
        "-DENABLE_OPENMP=ON"
        "-DINSTALL_DEVEL_HEADERS=ON"
      ];
      enableParallelBuilding = true;
      configurePhase = ''
        cmake -Bbuild ${toString specialCmakeFlags}
        cd build
      '';
    });

    # OK
    openmpi = self.callPackage ./pkgs/libs/openmpi/default.nix {
      libpsm2 = if self.stdenv.isLinux then self.libpsm2 else null;
      libfabric = if self.stdenv.isLinux then self.libfabric else null;
      cudatoolkit = if config.cuda then self.cudatoolkit else null;
    };

    # OK
    fftw = self.callPackage ./pkgs/libs/fftw/default.nix {
      inherit config;
      stdenv = stdenvPerf;
      mpi = self.MPI;
    };

    # OK
    mvapich2 = self.callPackage ./pkgs/libs/mvapich2/default.nix {
      inherit config;
      libpsm2 = if self.stdenv.isLinux then self.libpsm2 else null;
      libfabric = if self.stdenv.isLinux then self.libfabric else null;
    };

    # OK
    intelmpi = self.callPackage ./pkgs/libs/intelmpi/default.nix {};

    # OK
    chemps2 = self.callPackage ./pkgs/libs/chemps2/default.nix {
      stdenv = stdenvPerf;
    };

    # OK
    scalapack = self.callPackage ./pkgs/libs/scalapack/default.nix {
      mpi = self.MPI;
    };

    elpa = self.python3Packages.toPythonApplication self.python3Packages.elpa;

    # OK
    globalarrays = self.callPackage ./pkgs/libs/globalarrays/default.nix rec {
      inherit config;
      mpi = self.MPI;
      forceMPI3 = true;
      libpsm2 = if (config.network == "omnipath") then self.libpsm2 else null;
      libfabric = if (config.network == "omnipath") then self.libfabric else null;
      rdma-core = if (config.network == "infiniband") then self.rdma-core else null;
    };

    # OK
    arpack = self.callPackage ./pkgs/libs/arpack/default.nix {
      stdenv = stdenvPerf;
      mpi = self.MPI;
      bigInt = false;
    };

    # OK
    superlu = self.callPackage ./pkgs/libs/superlu/default.nix {
      stdenv = stdenvPerf;
    };

    # OK
    armadillo = self.callPackage ./pkgs/libs/armadillo/default.nix {
      stdenv = stdenvPerf;
    };

    # OK
    libxsmm = self.callPackage ./pkgs/libs/libxsmm/default.nix {
      inherit config;
      stdenv = stdenvPerf;
    };

    # Magic for making things like OpenGL work on non-NixOS
    nixGL =
      let
        repo = self.fetchFromGitHub {
          owner = "guibou";
          repo = "nixGL";
          rev = "fad15ba09de65fc58052df84b9f68fbc088e5e7c";
          sha256 = "1wc5gfj5ymgm4gxx5pz4lkqp5vxqdk2njlbnrc1kmailgzj6f75h";
        };
      in
        import "${repo}/default.nix" {
          nvidiaVersion = if config.nixGL != null then config.nixGL.nvidiaVersion else null;
        };

    # OK
    libvdwxc = self.callPackage ./pkgs/libs/libvdwxc/default.nix {
      stdenv = stdenvPerf;
      mpi = self.MPI;
    };

    # WiP
    libvori = self.callPackage ./pkgs/libs/libvori/default.nix { };

    # WiP
    /*
    madness = self.python3Packages.callPackage ./pkgs/libs/madness/default.nix {
      stdenv = stdenvPerf;
      mpi = self.MPI;
      cudatoolkit = if config.cuda then self.cudatoolkit else null;
    };
    */

    python3 = super.python3.override {
      packageOverrides = python-self: python-super: {
        # OK
        rmsd = python-self.callPackage ./pkgs/libs/python/rmsd/default.nix {};

        # OK
        qcelemental = python-self.callPackage ./pkgs/libs/python/qcelemental/default.nix {};

        # OK
        qcengine = python-self.callPackage ./pkgs/libs/python/qcengine/default.nix {};

        # OK
        pylibefp = python-self.callPackage ./pkgs/libs/python/pylibefp/default.nix {
          stdenv = stdenvPerf;
        };

        # OK
        mpi4py = python-super.mpi4py.override {
          mpi = self.MPI;
        };

        # OK
        libcint = python-self.callPackage ./pkgs/libs/libcint/default.nix {
          stdenv = stdenvPerf;
        };

        # OK
        qcint = python-self.libcint.overrideAttrs (oldAttrs: rec {
          pame = "qcint";
          src = self.fetchFromGitHub {
            owner = "sunqm";
            repo = "qcint";
            rev = "v${oldAttrs.version}";
            sha256 = "0iqqq568q9sxppr08rvmpyjq0n82pm04x9rxhh3mf20x1ds7ngj5";
          };
        });

        # OK
        pyscf = python-self.callPackage ./pkgs/libs/python/pyscf/default.nix {
          stdenv = stdenvPerf;
          libcint = if (self.stdenv.isx86_64 && self.stdenv.isLinux) then python-self.qcint else python-self.libcint;
        };

        # OK
        lime = python-self.callPackage ./pkgs/libs/python/lime/default.nix {};

        # OK
        pychemps2 = python-self.callPackage ./pkgs/libs/python/pychemps2/default.nix {};

        # OK
        gau2grid = python-self.callPackage ./pkgs/libs/gau2grid/default.nix { };

        # OK
        vtk_9 = python-self.toPythonModule (self.vtk_9.override {
          python = python-self.python;
          enablePython = true;
          enableQt = true;
        });

        # OK
        pegamoid = python-self.callPackage ./pkgs/apps/pegamoid/default.nix { };

        # WiP
        fsspec = python-super.fsspec.overrideAttrs (oldAttrs: rec {
          doCheck = false;
        });

        # OK
        psi4 = python-self.callPackage ./pkgs/apps/psi4/default.nix {
          mrcc = if config.mrcc then self.mrcc else null;
          libefp = self.libefp_p;
          libxc = self.libxc-4_3_3;
          gau2grid = python-self.gau2grid.overrideAttrs (oldAttrs: rec {
            version = "1.3.1";
            src = super.fetchFromGitHub {
              owner = "dgasmith";
              repo = "gau2grid";
              rev = "v" + version;
              sha256= "0zkfil7cxjip79wqvhljk1ifjq0cwxzx6wlxgp63b6wbagma0i12";
            };
          });
        };
        psi4Unstable = python-self.psi4.override {
          version = "01.11.2020";
          rev = "9b60184c5d161e4871c91ce29a44e3ac2c2a438e";
          sha256 = "1vh8dp3nw4fk1mnfv0w8ici1lzxyfn5han7hipqzsfxl75w76r18";
          libxc = self.libxc-4_3_3;
          gau2grid = python-self.gau2grid.overrideAttrs (oldAttrs: rec {
            version = "2.0.4";
            src = super.fetchFromGitHub {
              owner = "dgasmith";
              repo = "gau2grid";
              rev = "v" + version;
              sha256= "0qypq8iax0n6yfi4223zya468v24b60nr0x43ypmsafj0104zqa6";
            };
          });
        };

        # OK
        i-pi = python-self.callPackage ./pkgs/apps/i-pi/default.nix { };

        # TODO - running external turbomole does not work. Seems to be XTB problem.
        enso = python-self.callPackage ./pkgs/apps/enso/default.nix {
          orca = if config.orca then self.orca else null;
          turbomole = if config.turbomole then self.turbomole else null;
        };

        # OK
        gpaw = python-self.callPackage ./pkgs/apps/gpaw/default.nix {
          mpi = self.MPI;
          libxc = self.libxc-4_3_3;
        };

        # OK
        elpa = python-self.toPythonModule (python-self.callPackage ./pkgs/libs/elpa/default.nix {
          inherit config;
          stdenv = stdenvPerf;
          cudatoolkit = if config.cuda then self.cudatoolkit else null;
          mpi = self.MPI;
        });

        # TODO - wait for the next release to update this. Depends somehow on https://github.com/eljost/pysisyphus/pull/124
        pysisyphus =
          let
            version = "0.6";
            pname = "pysisyphus";

            repo = self.fetchFromGitHub {
              owner = "eljost";
              repo = pname;
              rev = version;
              sha256 = "0fjp2w38ib5hypshn2wfrvfr2wz7zxp4wakisj6x671l19lh6k6n";
            };

            pysis = python-self.callPackage "${repo}/nix/pysisyphus.nix" {
              orca = if config.orca then self.orca else null;
              turbomole = if config.turbomole then self.turbomole else null;
              gaussian = if config.gaussian then self.gaussian else null;
              gamess-us = if config.gamess-us then self.gamess-us else null;
              cfour = if config.cfour then self.cfour else null;
              molpro = if (config.molpro != null && config.molpro.enable) then self.molpro else null;
              mopac = null;
            };
          in
            pysis;

        pysisyphusUnstable =
          let
            version = "unstable";
            pname = "pysisyphus";

            repo = builtins.fetchTarball "https://github.com/eljost/pysisyphus/archive/master.tar.gz";

            pysis = python-self.callPackage "${repo}/nix/pysisyphus.nix" {
              orca = if config.orca then self.orca else null;
              turbomole = if config.turbomole then self.turbomole else null;
              gaussian = if config.gaussian then self.gaussian else null;
              gamess-us = if config.gamess-us then self.gamess-us else null;
              cfour = if config.cfour then self.cfour else null;
              molpro = if (config.molpro != null && config.molpro.enable) then self.molpro else null;
              mopac = null;
            };
          in
            pysis;
      };
    };
  }
