{ stdenv, fetchFromGitHub, makeWrapper, fetchpatch
# Configuration
, messyPsi4 ? true # Keep Psi4 files after last iteration
# Runtime Pograms
, gaussian ? null
, psi4
, nwchem
, tinker
}:
let
  version = "98ee89b7bb118d78492568126cd2de26031aad86";
in
  stdenv.mkDerivation rec {
    inherit version;
    pname = "lichem";

    src = fetchFromGitHub  {
      owner = "sheepforce";
      repo = "LICHEM";
      rev = version;
      sha256= "1xrw87122ixlssiigal6d003nq0mjrayabd2wi3lgbspyjzlmk2v";
    };

    patches = with stdenv.lib.lists; [
    ] ++ optional messyPsi4 (fetchpatch {
      name = "KeepPsi4Files";
      url = "https://github.com/sheepforce/LICHEM/commit/9e482de597721e3a40ab94fa7ac25c04b5d89dae.patch";
      sha256 = "0kjk4hfn1nj4gzcpqm4w57s95rqdi87qq92aqpb348yf2s43f6ay";
    });

    postPatch = ''
      substituteInPlace configure \
        --replace "CXXFLAGS= -static -O3 -fopenmp" "CXXFLAGS= -O3 -fopenmp"
    '';

    nativeBuildInputs = [
      makeWrapper
    ];

    postInstall = ''
      mkdir -p $out/bin
      mv $out/lichem $out/bin/.
    '';

    binSearchPath = stdenv.lib.strings.makeSearchPath "bin" ([
      psi4
      nwchem
      tinker
    ] ++ stdenv.lib.lists.optional (gaussian != null) gaussian
    );

    postFixup = ''
      wrapProgram $out/bin/lichem \
        --prefix PATH : "${binSearchPath}"
    '';

    meta = with stdenv.lib; {
      description = "QM/MM calculations with polarisable embedding.";
      license = licenses.gpl3;
      homepage = "https://github.com/CisnerosResearch/LICHEM";
      platforms = platforms.unix;
    };
  }
