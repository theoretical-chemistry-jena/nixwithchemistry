{ stdenv, fetchFromGitHub, autoreconfHook, symlinkJoin, config
# Dependencies
, blas
, lapack
, mvapich2
, scalapack
, libxc
, boost
, python
}:
# Make sure to have a correct version of MPI.
assert
  stdenv.lib.asserts.assertMsg
  (!blas.isILP64)
  "A 32 bit integer implementation of BLAS is required.";
let
  boostJoin = symlinkJoin {
    name = "boostJoin";
    paths = [
      boost
      boost.dev
    ];
  };

  scalapack_ = scalapack.override {
    inherit blas lapack;
    mpi = mvapich2;
  };
in
  stdenv.mkDerivation rec {
    pname = "bagel";
    version = "1.2.2";

    nativeBuildInputs = [
      autoreconfHook
      boost
      python
    ];

    buildInputs = [
      blas
      boost
      scalapack_
      libxc
    ];

    propagatedBuildInputs = [
      mvapich2
    ];

    src = fetchFromGitHub {
      owner = "qsimulate-open";
      repo = pname;
      rev = "v${version}";
      sha256= "184p55dkp49s99h5dpf1ysyc9fsarzx295h7x0id8y0b1ggb883d";
    };

    CXXFLAGS = with stdenv.lib.lists; toString ([
      "-DNDEBUG"
      "-O3"
      "-DCOMPILE_J_ORB"
      "-L${blas}/lib"
    ]
    ++ optional config.sse2 "-msse2"
    ++ optional config.sse3 "-msse3"
    ++ optional config.sse4 "-msse4"
    ++ optional config.sse4_2 "-msse4.2"
    ++ optional config.avx "-mavx"
    ++ optional config.avx2 "-mavx2"
    ++ optional config.avx512 "-mavx512"
    ++ optional (blas.passthru.implementation == "openblas") "-DZDOT_RETURN"
    );

    LDFLAGS = with stdenv.lib.lists; toString ([
      "-I${blas}/include"
    ]
    );

    BOOST_ROOT = boostJoin;

    preConfigure = ''
      configureFlagsArray+=(
        --with-libxc
        --with-boost=${boostJoin}
        --with-mpi=mvapich
        CPPFLAGS="-I${blas}/include"
        LDFLAGS="${LDFLAGS}"
        CXXFLAGS="${CXXFLAGS}"
      )
    '';

    postInstall = ''
      ln -s ${mvapich2}/bin/mpirun $out/bin/.
      ln -s ${mvapich2}/bin/mpiexec $out/bin/.
    '';

    enableParallelBuilding = true;

    meta = with stdenv.lib; {
      description = "Brilliantly Advanced General Electronic-structure Library";
      homepage = "https://nubakery.org";
      license = licenses.gpl3;
      platforms = platforms.unix;
    };
  }
