{ stdenv, makeWrapper, requireFile, autoPatchelfHook, fetchurl, writeTextFile
# Runtime dependencies
, openmpi
, glibc
}:

let
  openmpi_ = openmpi.overrideAttrs (oldAttrs: rec {
    version = "3.1.4";
    src = with stdenv.lib.versions; fetchurl {
      url = "https://download.open-mpi.org/release/open-mpi/v${major version}.${minor version}/${oldAttrs.pname}-${version}.tar.bz2";
      sha256 = "1mkrwd7qc189b5zg4jzrghagm7h7gp85pxqrq5yhqlyvah09x9hp";
    };
    configureFlags = oldAttrs.configureFlags ++ ["--disable-builtin-atomics" "--with-hwloc=internal"];
  });

  # Meta information
  pname = "orca";
  version = "4.2.1";
  description = "General purpose quantum chemistry program";
  homepage = "https://orcaforum.kofo.mpg.de/app.php/portal";

  pkgConfig = writeTextFile {
    name = "${pname}.pc";
    text = ''
      prefix=@out@
      exec_prefix=''${prefix}

      Name: ${pname}
      Description: ${description}
      Version: ${version}

      Requires: ${openmpi.pname} = ${openmpi.version}
      Libs:
      Cflags:
      URL: ${homepage}
    '';
  };

in
  stdenv.mkDerivation rec {
    inherit pname version;

    nativeBuildInputs = [
      makeWrapper
      autoPatchelfHook
    ];

    buildInputs = [
      openmpi_
      glibc
    ];

    src = requireFile rec {
      name = "orca_4_2_1_linux_x86-64_shared_openmpi314.tar.xz";
      sha256 = "a84b6d2706f0ddb2f3750951864502a5c49d081836b00164448b1d81c577f51a";
      message = ''
        The ORCA binaries are only available after registration and agreement to their license. Obtain
        your own license at https://orcaforum.kofo.mpg.de/app.php/portal and download the shared linux
        binaries with the filename ${name}. Add the ORCA
        archive to the nix-store by:
          nix-store --add-fixed sha256 ${name}
        and then rebuild.
      '';
    };

      #../../../proprietary/orca_4_2_1_linux_x86-64_shared_openmpi314.tar.xz;

    dontConfigure = true;
    dontBuild = true;
    installPhase = ''
      runHook preInstall


      # Move the ORCA libraries away from the unpacked src dir and install them in the lib directory.
      # In this way, they do not interfer with the installation step of the executables.
      mkdir -p $out/lib
      libs=$(find -type f -name "lib*")
      for l in $libs ; do mv $l $out/lib/. ; done

      # Copy all executables to the bin directory
      mkdir -p $out/bin
      bins=$(find -type f -executable)
      cp $bins $out/bin
      cp -r contrib $out/bin/.

      # Copy the license stuff to a doc directory
      mkdir -p $out/doc
      cp *.pdf $out/doc/.


      runHook postInstall
    '';

    postInstall = ''
      mkdir -p $out/lib/pkgconfig

      substitute ${pkgConfig} $out/lib/pkgconfig/${pkgConfig.name} \
        --subst-var "out"
    '';


    preFixup = ''
      exes=$(find $out/bin/ -type f -executable -not -path "contrib")
      for exe in $exes; do
        wrapProgram $exe \
          --prefix PATH : ${openmpi_}/bin
      done
    '';

    /*
    runtimeDependencies is a variable, that is used by autoPatchelfHook and adds those to the ELF
    binaries no matter what. This ensures here that ORCA can find libc and Openmpi stuff.
    */
    runtimeDependencies = [
      openmpi_
      glibc
    ];

    #passthru = { inherit openmpi; };

    meta = with stdenv.lib; {
      inherit description homepage;
      license = licenses.unfree;
      platforms = [ "x86_64-linux" ];
    };
  }
