{ stdenv, gfortran, fetchFromGitHub, makeWrapper, meson, ninja
, mkl
}:
stdenv.mkDerivation rec {
    pname = "xtb4stda";
    version = "1.0";

    nativeBuildInputs = [
      gfortran
      makeWrapper
      meson
      ninja
    ];

    buildInputs = [
      mkl
    ];

    src = fetchFromGitHub  {
      owner = "grimme-lab";
      repo = pname;
      rev = "v${version}";
      sha256 = "1sxki42pjjqiihklnlk8l2gaw1s0dp4wki80s666b672bihpk83b";
    };

    postFixup = ''
      wrapProgram $out/bin/xtb4stda \
        --set-default "OMP_STACKSIZE" "4G" \
        --set "XTB4STDAHOME" "$out/share/xtb4stda" \
        --run "ulimit -s unlimited"
    '';

    hardeningDisable = [
      "format"
    ];

    meta = with stdenv.lib; {
      description = " sTDA-xTB Hamiltonian for ground state ";
      license = licenses.lgpl3;
      homepage = "https://github.com/grimme-lab/xtb4stda";
      platforms = platforms.unix;
    };
  }
