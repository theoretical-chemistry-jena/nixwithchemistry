{ stdenv, gfortran, fetchurl, makeWrapper
, blas
, xtb4stda
, gawk
, gnused
, coreutils
}:
let
  version = "1.6.2";
  g2molden = fetchurl {
    url = "https://github.com/grimme-lab/stda/releases/download/v${version}/g2molden";
    sha256 = "1dhwvqvn3irnp7i3pknzcqjrm8bks2rfzpdgd4qlr09wb4727533";
  };
  qc2molden = fetchurl {
    url = "https://github.com/grimme-lab/stda/releases/download/v${version}/qc2molden.sh";
    sha256 = "1sylx27jfwy44a4i8bcsqma80g6xnycwh2q6w8ip1qrm2b0z27gv";
  };
in
  stdenv.mkDerivation rec {
    inherit version;
    pname = "stda";

    nativeBuildInputs = [
      gfortran
      makeWrapper
    ];

    buildInputs = [
      blas
    ];

    propagatedBuildInputs = [
      xtb4stda
      gawk
      gnused
      coreutils
    ];

    src = fetchurl  {
      url = "https://github.com/grimme-lab/${pname}/releases/download/v${version}/${pname}_v${version}";
      sha256 = "1n7ky1mvf58b1iyvm1x642vl6i3b95nimvcfy48jwsx2clgsdkd5";
    };

    dontUnpack = true;
    dontConfigure = true;
    dontBuild = true;

    installPhase = ''
      mkdir -p $out/bin

      # stda main executable
      cp $src stda
      chmod +x stda
      cp stda $out/bin/.

      # g2molden
      cp ${g2molden} g2molden
      chmod +x g2molden
      cp g2molden $out/bin/.

      # qc2molden.sh
      cp ${qc2molden} qc2molden.sh
      patchShebangs qc2molden.sh
      chmod +x qc2molden.sh
      cp qc2molden.sh $out/bin/.

      for exe in $out/bin/*; do
        wrapProgram $exe \
          --set-default "OMP_STACKSIZE" "4G" \
          --set "STDAHOME" "$out/bin"
      done
    '';

    meta = with stdenv.lib; {
      description = "stda program for computing excited states and response functions via simplified TD-DFT methods (sTDA, sTD-DFT, and SF-sTD-DFT) ";
      license = licenses.lgpl3;
      homepage = "https://github.com/grimme-lab/stda";
      platforms = platforms.unix;
    };
  }
