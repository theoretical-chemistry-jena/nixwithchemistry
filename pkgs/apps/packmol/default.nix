{ stdenv, gfortran, fetchFromGitHub }:
stdenv.mkDerivation rec {
    pname = "packmol";
    version = "20.010";

    buildInputs = [
      gfortran
    ];

    src = fetchFromGitHub {
      owner = "m3g";
      repo = pname;
      rev = version;
      sha256= "0njr6fbr7jg65bimm1hvjpx4b8076g5j59szl49shby6323b6aw9";
    };

    dontConfigure = true;

    patches = [
      ./patches/MakeFortran.patch
    ];

    installPhase = ''
      mkdir -p $out/bin
      cp -p packmol $out/bin
    '';

    hardeningDisable = [
      "format"
    ];

    meta = with stdenv.lib; {
      description = "Generating initial configurations for molecular dynamics";
      license = licenses.mit;
      homepage = "http://m3g.iqm.unicamp.br/packmol/home.shtml";
      platforms = platforms.unix;
    };
  }
