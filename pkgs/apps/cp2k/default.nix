{ stdenv, fetchFromGitHub, fetchFromGitLab, symlinkJoin, fetchurl
# Build time dependencies
, makeWrapper
, writeTextFile
, python3
, bash
, which
# Normal link dependencies
, gfortran
, blas
, lapack
, mpi
, elpa
, fftw
, libint
, libxc-4_3_3
, libxsmm
, scalapack
, libvori
}:
# Check for BLAS
assert stdenv.lib.asserts.assertMsg
  (!blas.isILP64)
  "A 32 bit integer implementation of BLAS is required.";

let
  arch = "Linux-x86-64-gfortran";
  cp2kVersion = "psmp";

  scalapack_ = scalapack.override {
    inherit mpi blas lapack;
  };

  elpa_ = (elpa.overrideAttrs (oldAttrs: rec {
    version = "2019.11.001";
    src = fetchurl {
      url = "https://elpa.mpcdf.mpg.de/html/Releases/${version}/${oldAttrs.pname}-${version}.tar.gz";
      sha256 = "0mzkmw2jsyc9y2yks8caj203avrvk7igfc2217hwf8rf0j7lldqh";
    };
  })).override {
    inherit mpi blas;
    scalapack = scalapack_;
    python = python3;
  };

  fftw_ = fftw.override {
    inherit mpi;
  };

  libxsmm_ = libxsmm.override {
    inherit blas;
  };

  fftwJoin = symlinkJoin {
    name = "fftwJoin";
    paths = [
      fftw_.all
    ];
  };

  libSearchPath = stdenv.lib.strings.makeSearchPath "lib" [
    blas
    mpi
    elpa_
    fftwJoin
    libint
    libxc-4_3_3
    libxsmm_
    scalapack_
    libvori
  ];

  archFile = writeTextFile {
    name = "${arch}.${cp2kVersion}";
    text = ''
      CC = ${mpi}/bin/mpicc -fopenmp
      FC = ${mpi}/bin/mpif90
      LD = ${mpi}/bin/mpif90
      AR = ar -r

      # Here we should include the plumed stuff

      ELPA_VER = ${elpa_.version}
      ELPA_INC = ${elpa_}/include/elpa_openmp-${elpa_.version}
      ELPA_LIB = ${elpa_}/lib

      FFTW_INC = ${fftwJoin}/include
      FFTW_LIB = ${fftwJoin}/lib

      LIBINT_INC = ${libint}/include
      LIBINT_LIB = ${libint}/lib

      LIBXC_INC = ${libxc-4_3_3}/include
      LIBXC_LIB = ${libxc-4_3_3}/lib

      LIBXSMM_INC = ${libxsmm_}/include
      LIBXSMM_LIB = ${libxsmm_}/lib

      SCALAPACK_LIB = ${scalapack_}/lib

      LIBVORI_LIB = ${libvori}/lib

      # Set CFLAGS here again? Or get them from stdenv?

      DFLAGS  = -D__ELPA -D__FFTW3 -D__LIBINT -D__LIBXC -D__LIBXSMM
      DFLAGS += -D__MPI_VERSION=3 -D__parallel -D__SCALAPACK -D__F2008
      DFLAGS += -D__MAX_CONTR=4 -D__LIBVORI

      FCFLAGS  = $(CFLAGS) $(DFLAGS)
      FCFLAGS += -ffree-form -ffree-line-length-none
      FCFLAGS += -fopenmp
      FCFLAGS += -ftree-vectorize -funroll-loops -std=f2008
      FCFLAGS += -I$(ELPA_INC)/elpa -I$(ELPA_INC)/modules
      FCFLAGS += -I$(FFTW_INC) -I$(LIBINT_INC) -I$(LIBXC_INC) -I$(LIBXSMM_INC)

      CFLAGS += -fopenmp

      LIBS  = -L${libSearchPath}
      LIBS += $(FFTW_LIB)/libfftw3.so $(FFTW_LIB)/libfftw3_omp.so
      LIBS += $(SCALAPACK_LIB)/libscalapack.so
      LIBS += $(LIBXC_LIB)/libxcf03.so $(LIBXC_LIB)/libxc.so
      LIBS += $(LIBXSMM_LIB)/libxsmmf.so $(LIBXSMM_LIB)/libxsmm.so
      LIBS += $(LIBINT_LIB)/libint2.so
      LIBS += $(ELPA_LIB)/libelpa_openmp.so
      LIBS += $(LIBVORI_LIB)/libvori.a
      LIBS += "-lblas"
      LIBS += -lstdc++
      LIBS += -fopenmp

      LDFLAGS = $(FCFLAGS) $(LIBS)
    '';
  };
in
  stdenv.mkDerivation rec {
    pname = "cp2k";
    version = "8.1.0";

    src = fetchFromGitHub  {
      owner = "cp2k";
      repo = pname;
      rev = "v${version}";
      sha256= "1qv7gprmm9riz9jj82n0mh2maij137h3ivh94z22bnm75az86jcs";
      fetchSubmodules = true;
    };

    nativeBuildInputs = [
      python3
      bash
      which
      makeWrapper
    ];

    buildInputs = [
      gfortran
      blas
      libvori
    ];

    propagatedBuildInputs = [
      mpi
    ];

    postPatch = ''
      substituteInPlace exts/dbcsr/.cp2k/Makefile --replace "/usr/bin/env python3" "${python3}/bin/python"
      substituteInPlace tools/build_utils/check_archives.py --replace "/usr/bin/env python3" "${python3}/bin/python"
      patchShebangs tools exts/dbcsr/tools/build_utils exts/dbcsr/.cp2k
    '';

    configurePhase = ''
      # Replace the architecture build file with our own.
      rm arch/${arch}.${cp2kVersion}
      cp ${archFile} arch/${arch}.${cp2kVersion}
    '';

    makeFlags = [
      "ARCH=${arch}"
      "VERSION=${cp2kVersion}"
    ];

    enableParallelBuilding = true;

    hardeningDisable = [
      "format"
    ];

    binSearchPath = stdenv.lib.strings.makeSearchPath "bin" [ mpi ];

    installPhase = ''
      # Prepare target
      mkdir -p $out/bin $out/lib $out/share/cp2k/data

      # Copy executables
      cp -r exe/${arch}/* $out/bin/.

      # Copy libraries (static only, unfortunately)
      cp -r lib/${arch}/${cp2kVersion}/* $out/lib/.

      # Copy data directory
      cp -r data/* $out/share/cp2k/data/.

      # Copy test directory for references
      cp -r tests $out/share/cp2k/.

      # Link the MPI executables to CP2K bin dir.
      ln -s ${mpi}/bin/mpirun $out/bin/.
      ln -s ${mpi}/bin/mpiexec $out/bin/.
    '';

    /*
    Make wrappers around CP2K.
    */
    postFixup = ''
      cd $out
      executables=$(find $out/bin/ -type f -executable)
      for exe in $executables; do
        wrapProgram $exe \
          --set-default CP2K_DATA_DIR $out/share/cp2k/data
      done
    '';

    doCheck = false;

    meta = with stdenv.lib; {
      description = "Quantum chemistry and solid state physics program";
      homepage = "https://www.cp2k.org";
      license = licenses.gpl3;
      platforms = [ "x86_64-linux" ];
    };
  }
