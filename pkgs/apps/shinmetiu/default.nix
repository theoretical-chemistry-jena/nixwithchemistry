{ stdenv, requireFile, unzip, gfortran, fftw, mkl, config ? null }:
let
  generalPerformanceFlags = [
    "-O3"
    "-mcmodel=medium"
    "-ftree-vectorize"
    "-fexternal-blas"
    "-floop-nest-optimize"
    "-floop-parallelize-all"
  ];
  architecturePerformanceFlags = if config == null then [] else (
    with stdenv.lib.lists;
    []
    ++ optional config.sse "-msse"
    ++ optional config.sse2 "-msse2"
    ++ optional config.sse3 "-msse3"
    ++ optional config.sse4 "-msse4"
    ++ optional config.sse4_2 "-msse4.2"
    ++ optional config.avx "-mavx"
    ++ optional config.avx2 "-mavx2"
    ++ optional config.avx512 "-mavx512"
    );
in
  stdenv.mkDerivation {
    version = "1jacob";
    pname = "shinmetiu";

    nativeBuildInputs = [
      unzip
    ];

    buildInputs = [
      gfortran
      fftw
      mkl
    ];

    src =
      let gitHash = "20e83b00a10191522003fc21032eeb11738107bc";
      in requireFile rec {
      name = "ShinMetiu-${gitHash}.zip";
      sha256 = "861f16c7a17a3bd9cdb02d89d267b9e5515b8c4722e17828fa696bc2784540f3";
      url = "https://github.com/sheepforce/ShinMetiu/tree/${gitHash}";
      message = ''
        The Shin-Metiu source code is not publically available, but a build-system patched version can
        be downloaded from ${url}. Obtain ${name} and add it to the nix-store by:
          nix-store --add-fixed sha256 ${name}
        and then rebuild.
      '';
    };

    unpackPhase = ''
      unzip $src
      cd ShinMetiu-*
    '';

    dontConfigure = true;

    preBuild = ''
      mkdir -p bin

      makeFlagsArray=(
        FFLAGS="${builtins.toString (generalPerformanceFlags ++ architecturePerformanceFlags)}"
        LDFLAGS=-L${fftw}/lib:${mkl}/lib
        INC=-I${if (builtins.hasAttr "dev" fftw) then fftw.dev else fftw}/include
      )
    '';

    hardeningDisable = [
      "format"
    ];

    installPhase = ''
      mkdir -p $out/bin
      cp -r control $out/bin/.
    '';
  }
