{ fetchFromGitHub, buildPythonPackage, lib, makeWrapper
, cefine
# QC Programs
, orca
, turbomole
, xtb
, crest
}:
assert
  lib.asserts.assertMsg
  ((orca != null) || (turbomole != null))
  "At least one of the quantum chemistry programs ORCA or Turbomole is required.";

let
  turbomole_ = turbomole.override {
    useMPI = false;
  };

  binSearchPath = lib.strings.makeSearchPath "bin" [
    cefine
    orca
    turbomole
    xtb
    crest
  ];
in
  buildPythonPackage rec {
    pname = "enso";
    version = "2.0.1";

    nativeBuildInputs = [
      makeWrapper
    ];

    propagatedBuildInputs = with lib; [
      xtb
      crest
    ]
    ++ lists.optional (orca != null) orca
    ++ lists.optionals (turbomole != null) [turbomole cefine]
    ;

    src = fetchFromGitHub  {
      owner = "grimme-lab";
      repo = pname;
      rev = "v${version}";
      sha256= "0dbrh1v2ipz4ph1ncxhl7azk5pnwiq569ya18si4h495zcrc30vg";
    };

    dontConfigure = true;
    dontBuild = true;

    patches = [
      ./patches/ensorc.patch
    ];

    postPatch = ''
      patchShebangs .
      patchShebangs scripts
    '';

    installPhase = with lib.strings; ''
      mkdir -p $out/bin

      chmod +x enso.py
      cp enso.py $out/bin/.

      chmod +x nmrplot.py
      cp nmrplot.py $out/bin/.

      # Create the ensorc file
      mkdir -p $out/share/enso
      $out/bin/enso.py -write_ensorc
      mv ensorc_new $out/share/enso/ensorc

      # Correct the ensorc settings
      substituteInPlace $out/share/enso/ensorc \
          --replace "GFN-xTB: /path/including/binary/xtb-binary"     "GFN-xTB: ${xtb}/bin/xtb"           \
          --replace "CREST: /path/including/binary/crest-binary"     "CREST: ${crest}/bin/crest"         \
        ${optionalString (orca != null) ''
          --replace "ORCA: /path/excluding/binary/"                  "ORCA: ${orca}/bin/"                \
          --replace "ORCA version: 4.2.1"                            "ORCA version: ${orca.version}"     \
        '' +
        optionalString (turbomole != null) ''
          --replace "mpshift: /path/including/binary/mpshift-binary" "mpshift: ${turbomole}/bin/mpshift" \
          --replace "escf: /path/including/binary/xtb-binary"        "escf: ${turbomole}/bin/escf"       \
        '' +
        (if (orca != null) then ''
          --replace "prog: None"                                     "prog: orca"
        ''
        else if (turbomole != null) then ''
          --replace "prog: None"                                     "prog: tm"
        ''
        else ""
        )
        }

      # Wrap the enso executable and set a path to ensorc
      wrapProgram $out/bin/enso.py \
        --set-default "ENSOPATH" "$out/share/enso/ensorc" \
        --set-default "PARA_ARCH" "SMP" \
        --prefix PATH : "${binSearchPath}"
    '';

    doCheck = false;

    meta = with lib; {
      description = "energetic sorting of conformer rotamer ensembles";
      license = licenses.lgpl3;
      homepage = "https://github.com/grimme-lab/enso";
      platforms = platforms.unix;
    };
}
