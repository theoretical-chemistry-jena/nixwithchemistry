# Taken with modifications from https://github.com/markuskowa/NixOS-QChem/blob/master/molpro/default.nix
{ stdenv, requireFile, fetchurl, autoPatchelfHook, python, hostname, makeWrapper, config, writeTextFile
, glibc
}:
assert
  stdenv.lib.asserts.assertMsg
  (stdenv.system == "x86_64-linux")
  "Molpro only runs on x86_64 linux systems.";

assert
  stdenv.lib.asserts.assertMsg
  (builtins.pathExists config.molpro.token)
  "Molpro requires a license file to be installed. Set the path to the token file in \"config.nix\"";

let
  token = builtins.path {
    path = config.molpro.token;
    name = "molpro.token";
  };

  # Meta information
  pname = "molpro";
  version = "2020.1.2";
  description = "Quantum chemistry program package";
  homepage = "https://www.molpro.net/";

  pkgConfig = writeTextFile {
    name = "${pname}.pc";
    text = ''
      prefix=@out@
      exec_prefix=''${prefix}

      Name: ${pname}
      Description: ${description}
      Version: ${version}

      Requires:
      Libs:
      Cflags:
      URL: ${homepage}
    '';
  };

in
  stdenv.mkDerivation rec {
    inherit pname version;

    src = requireFile {
      url = "http://www.molpro.net";
      name = "molpro-mpp-${version}.linux_x86_64_openmp.sh.gz";
      sha256 = "37b5ba6f8c01c68981d141891362e06cf0b3f2c98d7dea20fd113ec75d06d3c4";
    };

    nativeBuildInputs = [
      autoPatchelfHook
      hostname
      makeWrapper
    ];

    buildInputs = [
      python
    ];

    unpackPhase = ''
      mkdir -p source
      gzip -d -c $src > source/install.sh
      cd source
    '';

    postPatch = ''
      sed -i "1,/_EOF_/s:/bin/pwd:pwd:" install.sh
    '';

    configurePhase = ''
      export MOLPRO_KEY="$(cat ${token})"
    '';

    installPhase = ''
      runHook preInstall

      sh install.sh -batch -prefix $out

      runHook postInstall
    '';

    postInstall = ''
      mkdir -p $out/lib/pkgconfig

      substitute ${pkgConfig} $out/lib/pkgconfig/${pkgConfig.name} \
        --subst-var "out"
    '';

    postFixup = ''
      substituteInPlace $out/bin/molpro \
        --replace 'LAUNCHER="''${MOLPRO_PREFIX}/bin/mpiexec.hydra -machinefile %h -np %n %x"' 'LAUNCHER="''${MOLPRO_PREFIX}/bin/mpiexec.hydra -iface ${config.molpro.interface} -machinefile %h -np %n %x"' \

      wrapProgram $out/bin/molpro \
        --prefix PATH : "${hostname}/bin"
    '';

    runtimeDependencies = [
      glibc
    ];

    meta = with stdenv.lib; {
      inherit homepage description;
      license = licenses.unfree;
      platforms = [ "x86_64-linux" ];
    };
  }
