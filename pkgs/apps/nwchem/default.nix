{ stdenv, gfortran, fetchFromGitHub, writeTextFile
# Build utlities
, perl
, which
, python
, tcsh
, bash
, automake
, autoconf
, libtool
, makeWrapper
, gcc
, ncurses
# Dependencies
, blasILP
, lapack
, mpi
, openssh
}:
assert
  stdenv.lib.asserts.assertMsg
  (blasILP.isILP64 || blasILP.passthru.implementation == "mkl")
  "A 64 bit integer implementation of BLAS is required.";

assert
  stdenv.lib.asserts.assertMsg
  (mpi.pname == "mvapich2" || mpi.pname == "openmpi")
  ''
    NWChem requires MVAPICH2 or OpenMPI. Please use an override for NWChem
    if you decided for another implementation in the rest of the overlay.
    Alternatively figure out how to use the mpif90 command to get the MPI
    link information from your MPI implementation in the NWChem nix derivation
    and patch it.
  '';

let
  useMKL64 = blasILP.passthru.implementation == "mkl";
  mkl = blasILP.passthru.provider;
  blasLibs = if useMKL64
    then "-lmkl_gf_ilp64 -lmkl_gnu_thread -lmkl_core -lgomp -lpthread -lm -ldl"
    else "-lblas"
  ;

  /*
  Globalarrays would normally be downloaded at build time by NWchem but this is prevented by the nix
  sandbox, therefore we download it here beforehand.
  */
  versionGA = "5.7.2"; # Fixed by NWChem
  globalarraysSrc = fetchFromGitHub {
    owner = "GlobalArrays";
    repo = "ga";
    rev = "v${versionGA}";
    sha256 = "0c1y9a5jpdw9nafzfmvjcln1xc2gklskaly0r1alm18ng9zng33i";
  };

  /*
  NWChem needs an RC file at runtime. This is a skeleton that is fixed by the derivation later.
  */
  NWChemRC = writeTextFile {
    name = "nwchemrc";
    text = ''
      nwchem_basis_library @out@/share/nwchem/data/libraries/
      nwchem_nwpw_library @out@/share/nwchem//data/libraryps/
      ffield amber
      amber_1 @out@/share/nwchem/data/amber_s/
      amber_2 @out@/share/nwchem/data/amber_q/
      amber_3 @out@/share/nwchem/data/amber_x/
      amber_4 @out@/share/nwchem/data/amber_u/
      spce    @out@/share/nwchem/data/solvents/spce.rst
      charmm_s @out@/share/nwchem/data/charmm_s/
      charmm_x @out@/share/nwchem/data/charmm_x/
    '';
  };

  # Meta information

  description = "Open Source High-Performance Computational Chemistry";
  pname = "nwchem";
  version = "7.0.2";
  homepage = "http://www.nwchem-sw.org";

  pkgConfig = writeTextFile {
    name = "${pname}.pc";
    text =  ''
      prefix=@out@
      exec_prefix=''${prefix}

      Name: ${pname}
      Description: ${description}
      Version: ${version}

      Requires:
      Libs:
      Cflags:
      URL: ${homepage}
    '';
  };
in
  stdenv.mkDerivation rec {
      inherit pname version;

      nativeBuildInputs = [
        perl
        automake
        autoconf
        libtool
        makeWrapper
      ];

      buildInputs = [
        tcsh
        openssh
        which
        gfortran
        mpi
        (if useMKL64 then mkl else blasILP)
        which
        python
        ncurses
      ];

      src = fetchFromGitHub {
        owner = "nwchemgit";
        repo = "nwchem";
        rev = "v${version}-release";
        sha256 = "1ckhcjaw1hzdsmm1x2fva27c4rs3r0h82qivg72v53idz880hbp3";
      };

      /*
      Add the globalarrays source to the unpacke NWchem stuff.
      */
      postUnpack = ''
        cp -r ${globalarraysSrc}/ source/src/tools/ga-${versionGA}
        chmod -R u+w source/src/tools/ga-${versionGA}
      '';

      /*
      Replace interpreter path and assumed absolute paths to compilers and coreutils.
      */
      postPatch = ''
        find -type f -executable -exec sed -i "s:/bin/csh:${tcsh}/bin/tcsh:" \{} \;
        find -type f -name "GNUmakefile" -exec sed -i "s:/usr/bin/gcc:${gcc}/bin/gcc:" \{} \;
        find -type f -name "GNUmakefile" -exec sed -i "s:/bin/rm:rm:" \{} \;
        find -type f -executable -exec sed -i "s:/bin/rm:rm:" \{} \;
        find -type f -name "makelib.h" -exec sed -i "s:/bin/rm:rm:" \{} \;

        # Make the build system ignore the download of GlobalArrays, which is forbidden in the
        # sandbox and already added manually.
        echo -e '#!/bin/sh\n cd ga-${versionGA};autoreconf -ivf' > src/tools/get-tools-github

        # Patch all remaining shebangs.
        patchShebangs ./
      '';

      /*
      Works through the many custom make steps of Nwchem.
        1. Get linking information from MPI compiler.
        2. Make the config file
        3. Compile nwchem
        4. Obtain a printable version fortran module
        5. Link everyting together
      */
      buildPhase = ''
        export LIBMPI=${
          if      mpi.pname == "openmpi" then "$(mpif90 -showme:link)"
          else if mpi.pname == "mvapich2" then "$(mpif90 -show | cut -d' ' -f2-)"
          else ""
        }
        export NWCHEM_TOP="$(pwd)"
        cd src
        echo "ROOT: $NWCHEM_TOP"
        make nwchem_config

        make -j $NIX_BUILD_CORES

        cd $NWCHEM_TOP/src/util
        make version
        make
        cd $NWCHEM_TOP/src
        make link
      '';

      enableParallelBuilding = true;

      # NWChem configuration environment parameters.
      NWCHEM_TARGET = "LINUX64";
      NWCHEM_LONG_PATHS = "y";

      ARMCI_NETWORK = "MPI-MT";
      USE_MPI = "y";
      USE_MPIF = "y";

      NWCHEM_MODULES = "all python";

      USE_PYTHONCONFIG = "y";
      USE_PYTHON64 = "n";
      PYTHONLIBTYPE = "so";
      PYTHONHOME = "${python}";
      PYTHONVERSION = "${stdenv.lib.versions.majorMinor python.version}";

      BLASOPT = "-L${if useMKL64 then mkl else blasILP}/lib ${blasLibs}";
      BLAS_SIZE="8";

      LAPACK_LIB = BLASOPT;
      LAPACK_LIBS = LAPACK_LIB;
      LAPACK_SIZE = (if useMKL64 then "8" else "4");
      LAPACK_OPT = BLASOPT;

      MRCC_METHODS = "y";
      EACCSD = "y";
      IPCCSD = "y";

      installPhase = ''
        runHook preInstall


        mkdir -p $out/bin $out/share/nwchem

        # Copy the executable
        cp $NWCHEM_TOP/bin/LINUX64/nwchem $out/bin/.

        # Copy the NWchem data.
        cp -r $NWCHEM_TOP/src/data $out/share/nwchem/.
        cp -r $NWCHEM_TOP/src/basis/libraries $out/share/nwchem/data/.
        cp -r $NWCHEM_TOP/src/nwpw/libraryps $out/share/nwchem/data/.
        cp -r $NWCHEM_TOP/QA $out/share/nwchem/.

        # Get the RC file
        cp ${NWChemRC} $out/share/nwchem/nwchemrc
        substituteAllInPlace $out/share/nwchem/nwchemrc

        # Link the MPI executables to the nwchem bin directory, so that one module is enough to run.
        ln -s ${mpi}/bin/mpirun $out/bin/.
        ln -s ${mpi}/bin/mpiexec $out/bin/.

        runHook postInstall
      '';

      postInstall = ''
        mkdir -p $out/lib/pkgconfig

        substitute ${pkgConfig} $out/lib/pkgconfig/${pkgConfig.name} \
          --subst-var "out"
      '';

      hardeningDisable = [
        "format"
      ];

      postFixup = ''
        wrapProgram $out/bin/nwchem \
         --set-default NWCHEM_BASIS_LIBRARY "$out/share/nwchem/data/libraries/" \
         --prefix PATH : ${mpi}/bin
      '';

      meta = with stdenv.lib; {
        inherit description homepage;
        platforms = platforms.unix;
        license = {
          fullName = "Educational Community License, Version 2.0";
          url = "https://opensource.org/licenses/ECL-2.0";
          shortName = "ECL 2.0";
        };
      };
    }
