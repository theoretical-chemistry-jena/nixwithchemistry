{ stdenv, gfortran, fetchFromGitHub, makeWrapper
, bash
, turbomole
}:
assert
  stdenv.lib.asserts.assertMsg
  (turbomole != null)
  "Turbomole is required for this program to work.";

let
  binSearchPath = stdenv.lib.strings.makeSearchPath "bin" [
    bash
    turbomole
  ];

in
  stdenv.mkDerivation rec {
    pname = "cefine";
    version = "2.22";

    nativeBuildInputs = [
      gfortran
      makeWrapper
    ];

    propagatedBuildInputs = [
      turbomole
    ];

    src = fetchFromGitHub  {
      owner = "grimme-lab";
      repo = pname;
      rev = "v${version}";
      sha256= "1bshsv8xrxwcln50br2d2czpvgh80pr6k1pl1im4ch9vbpszvb96";
    };

    hardeningDisable = [
      "format"
    ];

    dontConfigure = true;

    buildPhase = ''
      gfortran -o cefine cefine.f90
    '';

    installPhase = ''
      mkdir -p $out/bin
      cp cefine $out/bin/.
    '';

    postFixup = ''
      wrapProgram $out/bin/cefine \
        --prefix PATH : "${binSearchPath}"
    '';

    meta = with stdenv.lib; {
      description = "Non-interactive command-line wrapper around turbomoles define";
      license = licenses.lgpl3;
      homepage = "https://github.com/grimme-lab/cefine";
      platforms = platforms.unix;
    };
  }
