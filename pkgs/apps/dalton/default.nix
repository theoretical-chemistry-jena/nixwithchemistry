{ stdenv, fetchgit, gfortran, cmake, makeWrapper, which, openssh
# Dependencies
, blas
, lapack
, mpi
# Python
, python3
}:
assert
  stdenv.lib.asserts.assertMsg
  (!blas.isILP64)
  "A 32 bit integer implementation of BLAS is required.";

stdenv.mkDerivation rec {
  pname = "dalton";
  version = "2018.2";

  nativeBuildInputs = [
    gfortran
    cmake
    python3
    makeWrapper
  ];

  buildInputs = [
    blas
    lapack
  ];

  propagatedBuildInputs = [
    mpi
    which
  ];

  # Many submodules are required and they are not fetched by fetchFromGitLab.
  src = fetchgit  {
    url = "https://gitlab.com/dalton/dalton.git";
    rev = version;
    sha256 = "06j1mcgmssbqcrcpzjkh8x0q09rxvwamp72jrlzfwzmjhrljm7f3";
    deepClone = true;
  };

  /*
  Cmake is required to build but adding it to the buildinputs then ignores the setup script.
  Therefore i call the script here manually but cmake is invoked by setup.
  */
  postPatch = ''
    patchShebangs .
  '';

  FC = "mpif90";
  CC = "mpicc";
  CXX = "mpicxx";

  configurePhase = ''
    ./setup --prefix=$out --mpi --omp && cd build
  '';

  enableParallelBuilding = true;

  hardeningDisable = [
    "format"
  ];

  /*
  Dalton does not care about bin lib share directory structures and puts everything in a single
  directory. Clean up the mess here.
  */
  postInstall = ''
    mkdir -p $out/bin $out/share/dalton
    for exe in dalton dalton.x; do
      mv $exe $out/bin/.
    done

    for dir in basis tools; do
      mv $dir $out/share/dalton/.
    done

    substituteInPlace $out/bin/dalton \
     --replace 'INSTALL_BASDIR=$SCRIPT_DIR/basis' "INSTALL_BASDIR=$out/share/dalton/basis"
  '';

  /*
  Make the MPI stuff available to the Dalton script. Direct exposure of MPI is not necessary.
  */
  postFixup = ''
    wrapProgram $out/bin/dalton \
      --prefix PATH : ${mpi}/bin \
      --prefix PATH : ${which}/bin \
      --prefix PATH : ${openssh}/bin

    wrapProgram $out/bin/dalton.x \
      --prefix PATH : ${mpi}/bin \
      --prefix PATH : ${which}/bin \
      --prefix PATH : ${openssh}/bin
  '';

  meta = with stdenv.lib; {
    description = "Quantum chemistry code specialised on exotic properties.";
    license = licenses.lgpl21;
    homepage = "https://daltonprogram.org/";
    platforms = platforms.unix;
  };
}
