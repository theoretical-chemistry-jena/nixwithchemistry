{ stdenv, makeWrapper, requireFile, autoPatchelfHook, unzip, writeScriptBin, fetchurl
, bash
, glibc
, libX11
, libGL
, motif
, nixGL ? null
}:
let
  nixGLWrapperScriptName = "nixGL-multiwfn";
  nixGLWrapperScript = writeScriptBin nixGLWrapperScriptName ''
    #!${bash}/bin/bash
    @nixGLScript@ @MultiwfnExe@ "$@"
  '';

in
  stdenv.mkDerivation rec {
    pname = "multiwfn";
    version = "3.7";

    nativeBuildInputs = [
      makeWrapper
      autoPatchelfHook
      unzip
    ];

    buildInputs = [
      glibc
      libX11
      libGL
      motif
    ];

    /*
    src = requireFile rec {
      name = "Multiwfn_3.7_bin_Linux.zip";
      url = "https://mega.nz/folder/HVcjECZS#rGG6dCO57AwpdCgYaQ3apg/file/yQlnwbCC";
      sha256 = "7a6573e6e340a5b24b755b676fb23dd51649924ab5b65643778591c1589c68f7";
      message = ''
        MultiWFN probably needs to be downloaded manually from ${url}.
        The original website is slow and the links die after some time. Add the archive to the nix store by:
          nix-store --add-fixed sha256 ${name};
        and then rebuild.
      '';
    };
    */
    src = fetchurl {
      url = "http://sobereva.com/multiwfn/misc/Multiwfn_${version}_bin_Linux.zip";
      sha256 = "1xv8kicc34c5fx1mddmm9a94j5nm7nr6yrsvfm5v59a0wgk76rbs";
    };

    dontConfigure = true;
    dontBuild = true;

    installPhase = ''
      mkdir -p $out/bin $out/share/multiwfn

      # Copy binary to $out
      chmod +x Multiwfn
      cp Multiwfn $out/bin/.

      # Copy examples and settings
      cp -r examples settings.ini $out/share/multiwfn/.

      # Symlink the settings.
      ln -s $out/share/multiwfn/settings.ini $out/bin/.
    '';

    # OpenGL on non-NixOS systems requires nearly always an impure OpenGL driver replacement.
    postFixup = stdenv.lib.strings.optionalString (nixGL != null) ''
      mv $out/bin/Multiwfn $out/bin/.Multiwfn-wrapped

      cp ${nixGL.nixGLDefault}/bin/nixGL $out/bin/.nixGL
      patchShebangs $out/bin/.nixGL

      cp ${nixGLWrapperScript}/bin/${nixGLWrapperScriptName} $out/bin/Multiwfn
      substituteInPlace $out/bin/Multiwfn \
        --subst-var-by nixGLScript $out/bin/.nixGL \
        --subst-var-by MultiwfnExe $out/bin/.Multiwfn-wrapped

      chmod +x $out/bin/Multiwfn
    '';

    meta = with stdenv.lib; {
      description = "Multifunctional wave function analyser.";
      license = licenses.bsd3;
      homepage = "http://sobereva.com/multiwfn/index.html";
      platforms = [ "x86_64-linux" ];
    };
  }
