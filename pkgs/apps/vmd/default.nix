# Adapted from https://github.com/markuskowa/NixOS-QChem/blob/master/vmd/default.nix
{ stdenv, requireFile, makeWrapper, writeScriptBin
, bash
, perl
, tcl-8_5
, tk-8_5
, netcdf
, libGLU
, xorg
, fltk
, vrpn
, flex
, bison
, libGL_driver
# Alternatively use the binaries if supported on the OS and enable CUDA
, config
, cudatoolkit
, useBin ? false
}:
assert stdenv.lib.asserts.assertMsg
  (if useBin then (stdenv.isLinux && stdenv.isx86_64 && config.cuda) else true)
  "The VMD binaries with require an x86_64 linux OS with CUDA support. If your system does not provide this, set useBin to false.";

let
  version = "1.9.3";
  homepage = "https://www.ks.uiuc.edu/Research/vmd/";

  srcSource = requireFile rec {
    url = homepage;
    name = "vmd-${version}.src.tar.gz";
    sha256 = "0a7ijps3qmp2qkz0ys31bd96dkz3vg1vdm0fa7z21minr16k3p2v";
    message = ''
      VMD requires a license to be downloaded. Please go to ${homepage} and register to obtain your
      own VMD license. After downloading version ${version} as source code, add the tarball to the
      nix-store by:
        nix-store --add-fixed sha256 ${name}
      and then rebuild.
    '';
  };

  srcBin = requireFile rec {
    url = homepage;
    name = "vmd-1.9.3.bin.LINUXAMD64-CUDA8-OptiX4-OSPRay111p1.opengl.tar.gz";
    sha256 = "9427a7acb1c7809525f70f635bceeb7eff8e7574e7e3565d6f71f3d6ce405a71";
    message = ''
      VMD requires a license to be downloaded. Please go to ${homepage} and register to obtain your
      own VMD license. After downloading version ${version} as binary ("LINUX_64 OpenGL, CUDA, OptiX, OSPRay"),
      add the tarball to the nix-store by:
        nix-store --add-fixed sha256 ${name}
      and then rebuild.
    '';
  };

  meta = with stdenv.lib; {
    inherit homepage;
    description = "Molecular dyanmics visualisation program";
    license = licenses.unfree;
    platforms = platforms.linux;
  };

  /*
  Build the VMD plugins as their own derivation from source.
  */
  pluginsSrc = stdenv.mkDerivation {
    pname = "vmd-plugins";
    inherit version meta;
    src = srcSource;

    buildInputs = [
      tcl-8_5
      tk-8_5
      netcdf
    ];

    #postPatch = ''
    #'';

    sourceRoot = "plugins";

    makeFlags = [
      "LINUXAMD64"
    ];

    preBuild = ''
      export PLUGINDIR=$out/
    '';

    installTargets = "distrib";

    enableParallelBuilding = false;
  };

  /*
  VMD build from source.
  */
  vmdSource = stdenv.mkDerivation rec {
    pname = "vmd";
    inherit version meta;
    src = srcSource;

    nativeBuildInputs = [
      perl
      makeWrapper
    ];

    buildInputs = [
      pluginsSrc
      libGLU
      xorg.libX11
      xorg.libXinerama
      xorg.libXi
      tcl-8_5
      tk-8_5
      netcdf
      fltk
      vrpn
      flex
      bison
      libGL_driver
    ];

    postPatch = ''
      ln -s ${pluginsSrc} plugins
      substituteInPlace ./configure \
        --replace '/usr/local' "$out" \
        --replace '-ll' '-lfl'
    '';

    sourceRoot = "vmd-${version}";

    # non-standard configure script
    configurePhase = ''
      patchShebangs ./configure

      ./configure \
        LINUXAMD64 \
        OPENGL \
        OPENGLPBUFFER \
        FLTK \
        TK \
        IMD \
        XINERAMA \
        XINPUT \
        VRPN \
        NETCDF \
        COLVARS \
        TCL \
        PTHREADS \
        SILENT \
        GCC
    '';

    preBuild = ''
      cd src
    '';

    enableParallelBuilding = true;
  };

  /*
  VMD binary installed.
  */
  vmdBin = stdenv.mkDerivation rec {
    pname = "vmd";
    inherit version meta;

    src = srcBin;

    nativeBuildInputs = [
      perl
      makeWrapper
    ];

    buildInputs = [
      libGLU
      xorg.libX11
      xorg.libXinerama
      xorg.libXi
      tcl-8_5
      tk-8_5
      netcdf
      fltk
      vrpn
      flex
      bison
      libGL_driver
      cudatoolkit
    ];

    postPatch = ''
      substituteInPlace ./configure \
        --replace '/usr/local' "$out" \
        --replace '-ll' '-lfl'
    '';

    sourceRoot = "vmd-${version}";

    # non-standard configure script
    configurePhase = ''
      patchShebangs ./configure

      ./configure
    '';

    dontBuild = true;

    preInstall = ''
      cd src
    '';

    postFixup = ''
      wrapProgram $out/bin/vmd \
        --set "LC_ALL" "C"
    '';

    enableParallelBuilding = true;
  };

in if useBin then vmdBin else vmdSource
