{ stdenv, fetchFromGitHub, gfortran, makeWrapper
, blasILP
, lapack
}:
assert
  stdenv.lib.asserts.assertMsg
  (blasILP.isILP64 || blasILP.passthru.implementation == "mkl")
  "64 bit integer BLAS implementation required.";

let
  useMKL64 = blasILP.passthru.implementation == "mkl";
  mkl = blasILP.passthru.provider;
  blasLibs = if useMKL64
    then "-lmkl_intel_ilp64 -lmkl_gnu_thread -lmkl_core -lgomp -lpthread -lm -ldl"
    else "-lblas"
    ;

in
  stdenv.mkDerivation rec {
    pname = "wfoverlap";
    version = "1.0";

    src =
      let
        repo = fetchFromGitHub {
          owner = "sharc-md";
          repo = "sharc";
          rev = "d943ec7aff0fb6c81f61d3c057b0921d053e9e20";
          sha256 = "1a9frnxvm1jg4cv3jd4lm3q8m7igyc7fsp3baydfjkvk0b4ss9bc";
        };
      in "${repo}/wfoverlap/source";

    nativeBuildInputs = [
      gfortran
      makeWrapper
    ];

    buildInputs = [
      gfortran
      (if useMKL64 then mkl else blasILP)
      lapack
    ];

    patches = [
      ./patches/Makefile.patch
    ];

    postPatch = ''
       export blasLibs="${blasLibs}"
       export lapackLibs="-llapack"
       ls -lah ./Makefile
        substituteInPlace Makefile \
          --subst-var blasLibs \
          --subst-var lapackLibs
    '';

    dontConfigure = true;

    hardeningDisable = [
      "format"
    ];

    installPhase = ''
      cat Makefile
      mkdir -p $out/bin
      cp wfoverlap.x $out/bin/.
    '';

    meta = with stdenv.lib; {
      description = "Efficient calculation of wavefunction overlaps";
      license = licenses.gpl3;
      homepage = "https://sharc-md.org/?page_id=309";
      platforms = platforms.unix ;
    };
  }
