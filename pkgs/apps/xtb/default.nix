{ stdenv, gfortran, fetchFromGitHub, cmake, makeWrapper, writeTextFile
# Dependencies
, blas
, lapack
, turbomole ? null
, orca ? null
, cefine ? null
}:
let
  binSearchPath = with stdenv.lib.lists; stdenv.lib.strings.makeSearchPath "bin" ([ ]
    ++ optional (turbomole != null) turbomole
    ++ optional (orca != null) orca
    ++ optional (turbomole != null) cefine
  );

  useMKL = blas.passthru.implementation == "mkl";
  mkl = blas.passthru.provider;

  # Meta information
  pname = "xtb";
  version = "6.4.0";
  description = "Semiempirical Extended Tight-Binding Program Package";
  homepage = "https://www.chemie.uni-bonn.de/pctc/mulliken-center/grimme/software/xtb";

  pkgConfig = writeTextFile {
    name = "${pname}.pc";
    text =  ''
      prefix=@out@
      exec_prefix=''${prefix}
      libdir=''${exec_prefix}/lib
      sharedlibdir=''${libdir}
      includedir=''${prefix}/include

      Name: ${pname}
      Description: ${description}
      Version: ${version}

      Requires:
      Libs: -L''${libdir} -lxtb
      Cflags: -I''${includedir}
      URL: ${homepage}
    '';
  };

in
  stdenv.mkDerivation rec {
    inherit pname version;

    nativeBuildInputs = [
      gfortran
      cmake
      makeWrapper
    ];

    buildInputs = if useMKL then [
      mkl
    ] else [
      blas
      lapack
    ];

    src = fetchFromGitHub  {
      owner = "grimme-lab";
      repo = pname;
      rev = "v${version}";
      sha256= "0fcf9f6y93aii907as25vmchfvdyzyrk0w7nqwyv1mjvab9a9acc";
    };

    hardeningDisable = [
      "format"
    ];

    postFixup = ''
      wrapProgram $out/bin/xtb \
        --prefix PATH : "${binSearchPath}" \
        --set-default XTBPATH "$out/share/xtb"
    '';

    meta = with stdenv.lib; {
      inherit description homepage;
      license = licenses.lgpl3;
      platforms = platforms.unix;
    };
  }
