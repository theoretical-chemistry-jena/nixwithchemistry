{ stdenv, fetchurl, cmake, symlinkJoin
# Dependencies
, blas
, mpi ? null
, python
, boost
# Settings
, openmp ? (mpi == null)
}:
assert
  stdenv.lib.asserts.assertMsg
  (!blas.isILP64)
  "A 32 bit integer implementation of BLAS is required.";

let
  boost_ = boost.override {
    enableSingleThreaded = true;
    enableMultiThreaded = true;
    taggedLayout = true;
  };
  useBoost56 = with stdenv.lib.versions; with stdenv.lib.strings;
    if ((toInt (minor boost.version)) >= 56)
      then "yes"
      else "no"
  ;

  useMPI = if mpi == null then "no" else "yes";
  useOpenMP = if openmp then "yes" else "false";

  boostJoin = symlinkJoin {
    name = "boostJoin";
    paths = [
      boost_
      boost_.dev
    ];
  };
in
  stdenv.mkDerivation rec {
      pname = "block";
      version = "1.5.3";

      nativeBuildInputs = [
        cmake
        python
      ];

      buildInputs = with stdenv.lib.lists; [
        blas
        boostJoin
      ]
      ++ optional (mpi != null) mpi
      ;

      src = fetchurl  {
        url = "http://www.sunqm.net/pyscf/files/src/${pname}-${version}.tar.gz";
        sha256 = "0f8f97f3983f7b938d94470732884cd28e08589dd116a2e0b3dc62664a0b91d9";
      };

      /*
      Leads to a boost linking error otherwise.
      */
      postPatch = ''
        substituteInPlace Stackwavefunction.C \
          --replace '#define BOOST_NO_CXX11_SCOPED_ENUMS' '//#define BOOST_NO_CXX11_SCOPED_ENUMS'
      '';

      /*
      Configuration happens by editing the Makefile ...
      */
      configurePhase = ''
        substituteInPlace Makefile \
          --replace "CXX = clang++" "CXX = g++" \
          --replace "MPICXX = mpiicpc" "MPICXX = mpicxx" \
          --replace "BOOSTINCLUDE = /opt/local/include" "BOOSTINCLUDE = ${boostJoin}/include" \
          --replace "USE_BOOST56 = no" "USE_BOOST56 = ${useBoost56}" \
          --replace "BOOSTLIB = -L/opt/local/lib  -lboost_system-mt -lboost_filesystem-mt -lboost_serialization-mt" "BOOSTLIB = ${boostJoin}/lib/libboost_system-mt.so ${boostJoin}/lib/libboost_filesystem-mt.so ${boostJoin}/lib/libboost_serialization-mt.so" \
          --replace "USE_MPI = no" "USE_MPI = ${useMPI}" \
          --replace "OPENMP = no" "OPENMP = ${useOpenMP}" \
          --replace "LAPACKBLAS =    /usr/lib/liblapack.dylib /usr/lib/libblas.dylib" "LAPACKBLAS = -lblas" \
          --replace "OPT = -DNDEBUG -O2 -g -funroll-loops -Werror" "OPT = -DNDEBUG -O2 -g -funroll-loops -Werror -Wno-error=return-type" \
          --replace 'LIBS +=  -L$(NEWMATLIB) -lnewmat $(BOOSTLIB) $(LAPACKBLAS) $(MALLOC)' 'LIBS +=  -L$(NEWMATLIB) -lnewmat $(BOOSTLIB) $(LAPACKBLAS) $(MALLOC) -lpthread -lrt'
      '';

      hardeningDisable = [
        "all"
      ];

      installPhase = with stdenv.lib.strings; ''
        mkdir -p $out/bin
        cp block.spin_adapted $out/bin/.
      '' + optionalString (mpi != null) ''
        # Link the MPI executables to BLOCK bin dir.
        ln -s ${mpi}/bin/mpirun $out/bin/.
        ln -s ${mpi}/bin/mpiexec $out/bin/.
      '';

      enableParallelBuilding = true;

      meta = with stdenv.lib; {
        description = "BLOCK implements the density matrix renormalization group (DMRG) algorithm for quantum chemistry";
        license = licenses.gpl3;
        homepage = "https://sanshar.github.io/Block/";
        platforms = platforms.unix;
      };
    }
