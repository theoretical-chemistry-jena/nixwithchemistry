{ stdenv, requireFile, writeTextFile
# Dependencies
, blasILP
, openmpi ? null
}:
# Make sure to have a correct version of BLAS.
assert
  stdenv.lib.asserts.assertMsg
  (blasILP.isILP64 || blasILP.passthru.implementation == "mkl")
  "A 64 bit integer implementation of BLAS is required.";
let
  useMPI = openmpi != null;
  useMKL64 = blasILP.passthru.implementation == "mkl";
  mkl = blasILP.passthru.provider;

  # Meta information
  pname = "cfour";
  version = "2.1";
  description = "Specialist coupled cluster software.";
  homepage = "http://slater.chemie.uni-mainz.de/cfour/index.php";

  pkgConfig = writeTextFile {
    name = "${pname}.pc";
    text =  ''
      prefix=@out@
      exec_prefix=''${prefix}
      libdir=''${exec_prefix}/lib
      sharedlibdir=''${libdir}
      includedir=''${prefix}/include

      Name: ${pname}
      Description: ${description}
      Version: ${version}

      Requires:
      Libs: -L''${libdir} -lbox -lecctrip -lecp -leom -llibr -llibrcc -llibrsrcc -llibtdcc -lsdcctrip
      Cflags:
      URL: ${homepage}
    '';
  };



in
  stdenv.mkDerivation rec {
    inherit pname version;


    buildInputs = [
      (if useMKL64 then mkl else blasILP)
    ];

    propagatedBuildInputs = stdenv.lib.lists.optional useMPI openmpi;

    src = requireFile rec {
      name = "cfour-public-v${version}.tar.bz2";
      sha256 = "d88c1d7ca360f12edeca012a01ab9da7ba4792ee43d85c178f92fbe83cbdcdc4";
      url = "https://cfour.chem.ufl.edu/cfour-public/cfour-public/-/tree/v$[version}";
      message = ''
        The CFour source code is not publically available. Obtain your own license as described at
        http://slater.chemie.uni-mainz.de/cfour/index.php?n=Main.Download
        the source code at ${url}. Add the archive ${name} to the nix
        store by:
          nix-store --add-fixed sha256 ${name}
        and then rebuild.
      '';
    };

    preConfigure = ''
      configureFlagsArray+=(
        ${if useMPI then "--enable-mpi=openmpi" else ""}
        ${if useMPI then "--with-mpirun=\"${openmpi}/bin/mpiexec -np $CFOUR_NUM_CORES\"" else ""}
        ${if useMPI then "--with-exenodes=\"${openmpi}/bin/mpiexec -np $CFOUR_NUM_CORES\"" else ""}
      )
    '';

    postInstall = ''
      mkdir -p $out/lib/pkgconfig
      substitute ${pkgConfig} $out/lib/pkgconfig/${pkgConfig.name} \
        --subst-var "out"
    '';

    hardeningDisable = [
      "format"
    ];

    meta = with stdenv.lib; {
      license = licenses.unfree;
      platforms = platforms.unix;
    };
  }
