/*
NECI and chemps2 support are currently not working as hdf5.mod from hdf5-fortran cannot be found
correctly.
*/

{ stdenv, fetchFromGitLab, fetchFromGitHub, symlinkJoin, writeTextFile
# Build time dependencies
, makeWrapper
, cmake
, perl
, gfortran
, which
# Normal link dependencies
, blasILP
, fftw
, mpi ? null
, globalarrays
, hdf5-cpp
, hdf5-fortran
, armadillo
# Python
, python3
# Runtime dependencies
, openssh
}:
# Check for 64bit integer BLAS
assert
  stdenv.lib.asserts.assertMsg
  (blasILP.isILP64 || blasILP.passthru.implementation == "mkl")
  "A 64 bint integer BLAS implementation is required.";

assert
  stdenv.lib.asserts.assertMsg
  (builtins.elem blasILP.passthru.implementation [ "openblas" "mkl" ])
  "OpenMolcas requires OpenBLAS or MKL.";

let
  pythonWP = python3.withPackages (p: with p; [
    six
    pyparsing
  ]);


  blasProvider = blasILP.passthru.provider;
  blasImplementation = blasILP.passthru.implementation;

  mpiParallel = mpi != null;

  globalarrays_ = globalarrays.override {
    inherit mpi;
    blas = blasILP;
    forceMPI3 = true;
    bigInt = true;
    scalapack = null;
  };

  armadillo_ = armadillo.override {
    blas = blasILP;
  };

  fftw_ = fftw.override {
    inherit mpi;
  };

  hdf5-join = symlinkJoin {
    name = "hdf5-join";
    paths = [
      hdf5-cpp
      hdf5-cpp.dev
      hdf5-fortran
      hdf5-fortran.dev
    ];
  };

  /*
  The submodules of OpenMolcas enable additional features and are configured to be enabled in the
  configure step further down. Unfortunately the cannot be fetched by a fetchgit with submodules
  enabled, as the NECI submodule is broken (the commit does not exist anymore). Therefore the
  snapshots, that are referenced by OpenMolcas at v19.11 are referenced here and downloaded by nix.
  They must be put into the OpenMolcas repo later.
  */
  externalNECI = fetchFromGitHub {
    owner = "ghb24";
    repo = "NECI_STABLE";
    rev = "fba2da711cae5240f76c187a65dc24c1e7e354f0"; # Not exactly the one from OpenMolcas, as it does not exist!
    sha256 = "1fhjsnqzz5jvv4608idm5vlq8k3vzissgb69lyg9qw7cn2a39db5";
  };

  externalEfp = fetchFromGitHub {
    owner = "ilyak";
    repo = "libefp";
    rev = "7d1ff5fd2bd3c26a411473a1853d0ad37d823e61";
    sha256 = "10ippbwkch743q8p6ya57anai805ph1spi8s7vr5h7zsnpxi06bg";
  };

  externalLibmsym = fetchFromGitHub {
    owner = "mcodev31";
    repo = "libmsym";
    rev = "0c47befe4a1cd05cbba1aa561b914be926e5ced7";
    sha256 = "1cvcfx3wagcbycjm540snzyp8hxzm0h65y9h36jaizy34wy5f1xw";
  };

  externalWfa = fetchFromGitHub {
    owner = "libwfa";
    repo = "libwfa";
    rev = "eb2f1b990209a07497b05e69fcf1e00c4fe6afc2";
    sha256 = "1ra634qyvjxgvla1mwajavijjki7p1bf0ardllvkmfpa39ijv5g5";
  };

  # Meta data
  pname = "openmolcas";
  version = "20.10";
  description = "OpenMolcas is a quantum chemistry software package. The key feature of OpenMolcas is the multiconfigurational approach to the electronic structure.";
  homepage = "https://gitlab.com/Molcas/OpenMolcas";

  pkgConfig = writeTextFile {
    name = "${pname}.pc";
    text = ''
      prefix=@out@
      exec_prefix=''${prefix}
      libdir=''${exec_prefix}/lib
      sharedlibdir=''${libdir}
      includedir=''${prefix}/include

      Name: ${pname}
      Description: ${description}
      Version: ${version}

      Requires: ${mpi.pname} = ${mpi.version}
      Libs: -L''${libdir} -lmolcas -lwfa_molcas
      Cflags:
      URL: ${homepage}
    '';
  };




in
  stdenv.mkDerivation rec {
    inherit pname version;

    /*
    Molcas updates release tags and treats them as if they would be a branch. This breaks sha256
    hashes, therefor i pin a commit of this "branch" here ...
    */
    src = fetchFromGitLab  {
      owner = "Molcas";
      repo = "OpenMolcas";
      rev = "01fe7a2bdca51c51d183f3061b0ee9c631bf9bec";
      sha256= "0xr9plgb0cfmxxqmd3wrhvl0hv2jqqfqzxwzs1jysq2m9cxl314v";
    };

    postUnpack = ''
      # Manually initialise git submodules.
      cp -r ${externalNECI}/* source/External/NECI/.
      cp -r ${externalEfp}/* source/External/efp/.
      cp -r ${externalLibmsym}/* source/External/libmsym/.
      cp -r ${externalWfa}/* source/External/libwfa/.

      chmod -R +rwx source/External
    '';

    patches = [
      ./patches/MKLPath.patch # Patches CMakes strict assumptions on MKL directory layout and which BLACS lib to use for MVAPICH2
      ./patches/EFP_naming.patch # Patches double line
    ];

    # postPatch = ''
    #   # Patch interpreters in NECI
    #   sed -i "1s:/.*:/usr/bin/env python2:" External/NECI/tools/*.py
    #   patchShebangs External
    # '';

    nativeBuildInputs = [
      pythonWP
      cmake
      perl
      gfortran
      makeWrapper
      perl
      which
    ];

    buildInputs = with stdenv.lib.lists; [
      gfortran
      blasProvider
      hdf5-join
      armadillo_
      fftw_
      pythonWP
    ];

    propagatedBuildInputs = with stdenv.lib;
      lists.optionals mpiParallel [ mpi globalarrays_ openssh ];

    preConfigure = with stdenv.lib; ''
      cmakeFlagsArray+=(
        -DBUILD_SHARED_LIBS=ON
        -DOPENMP=ON
        -DTOOLS=ON
        -DHDF5=ON
        -DBUILD_SHARED_LIBS=ON
        -DFDE=ON
        -DEXPERT=ON
        -DTOOLS=ON
        -DWFA=ON
        -DMSYM=ON
        -DNECI=OFF
        -DEFPLIB=ON
        ${strings.optionalString mpiParallel "-DGA=ON -DMPI=ON"}
        ${if      blasImplementation == "openblas" then "-DLINALG=OpenBLAS -DOPENBLASROOT=${blasProvider}"
          else if blasImplementation == "mkl" then "-DLINALG=MKL -DMKLROOT=${blasProvider}"
          else null
        }
      )
    '';
    /*


    -DDMRG=ON
    -DQCMaquis_NAME="${config.qcmaquis.name}"
    -DQCMaquis_EMAIL="${config.qcmaquis.email}"
    -DNEVPT2=OFF

    -DBLOCK=ON
    -DBLOCK_DIR=${block}/lib

    -DCHEMPS2=OFF
    -DCHEMPS2_DIR=${chemps2}/bin
    */

    # GAROOT must be set as an environment variable if GA is enabled.
    GAROOT = globalarrays_;

    /*
    PyMolcas is already installed during building strangely ... Therefore the out directory must
    already exist.
    */
    postConfigure = ''
      mkdir -p $out/bin
      export PATH=$PATH:$out/bin
    '';

    enableParallelBuilding = true;

    hardeningDisable = [
      "format"
    ];

    /*
    Make sure the verify script is also present after installation.
    Also copy the test directory to $out.

    Also the external module libraries are missing. They must be copied manually.
    */
    postInstall = ''
      mkdir -p $out/share $out/lib/pkgconfig

      cp sbin/verify $out/bin/.

      cp -r ../test $out/share/.

      substitute ${pkgConfig} $out/lib/pkgconfig/${pkgConfig.name} \
        --subst-var "out"

      # Copy missing libraries.
      cp lib/libmsym.so* $out/lib/.
      cp External/src/efplib-build/libefp.so $out/lib/.
    '';

    binSearchPath = with stdenv.lib.lists; stdenv.lib.strings.makeSearchPath "bin" ([
      mpi
      openssh
    ]);
    # libSearchPath = with stdenv.lib; strings.makeSearchPath "lib/${python.executable}/site-packages" [
    #   pyparsing
    #   six
    # ];

    postFixup = ''
      # Wrong store path in shebang (no Python pkgs), force re-patching
      sed -i "1s:/.*:/usr/bin/env python3:" $out/bin/pymolcas
      patchShebangs $out/bin

      for exe in $(find $out/bin/* -type f -executable) $out/bin/pymolcas; do
        wrapProgram $exe \
          --prefix PATH : ${binSearchPath} \
          --set MOLCAS $out
      done
    '';

    doCheck = false;

    checkPhase = ''
      echo "I am now at: $(pwd)"
      echo "With contents: $(ls -lah)"
      echo "Contents of sbin: $(ls -lah sbin)"
      echo "Pymolcas at: $(which pymolcas)"
      export PATH=$(pwd)/bin:$(pwd)/sbin:$PATH
      pymolcas verify
    '';

    meta = with stdenv.lib; {
      inherit description homepage;
      license = licenses.lgpl2;
      platforms = platforms.unix;
    };
  }
