{ stdenv, gfortran, fetchFromGitLab, perl, symlinkJoin, autoreconfHook, config, which, pkgconfig
# Dependencies
, blas
, lapack
, mpi
, libxc
, gsl
, fftw
, libyaml
, libvdwxc
, hdf5
, netcdf
, netcdffortran
, scalapack
, elpa
, arpack
, parallel ? true
}:
assert
  stdenv.lib.asserts.assertMsg
  (!blas.isILP64)
  "A 32 bit integer implementation of BLAS is required.";

let
  fftwJoin = symlinkJoin {
    name = "fftwJoin";
    paths = [
      fftw_
      fftw_.dev
    ];
  };

  netcdfJoin = symlinkJoin {
    name = "netcdfJoin";
    paths = [
      netcdf_
      netcdffortran_
    ];
  };

  fftw_ = fftw.override {
    inherit mpi;
  };

  libvdwxc_ = libvdwxc.override {
    inherit mpi;
    fftw = fftw_;
  };

  hdf5_ = hdf5.override {
    inherit mpi;
  };

  netcdf_ = netcdf.override {
    hdf5 = hdf5_;
  };

  netcdffortran_ = netcdffortran.override {
    hdf5 = hdf5_;
    netcdf = netcdf_;
  };

  scalapack_ = scalapack.override {
    inherit mpi blas lapack;
  };

  elpa_ = elpa.override {
    inherit mpi blas;
  };

  arpack_ = arpack.override {
    inherit mpi blas lapack;
    bigInt = false;
  };

in
  stdenv.mkDerivation rec {
    pname = "octopus";
    version = "10.0";

    nativeBuildInputs = [
      gfortran
      perl
      autoreconfHook
      which
      pkgconfig
    ];

    buildInputs = [
      libyaml
      blas
      fftwJoin
      libxc
      gsl
      libvdwxc_
      netcdfJoin
      elpa_
      scalapack_
      arpack_
    ];

    propagatedBuildInputs = stdenv.lib.lists.optional parallel mpi;

    src = fetchFromGitLab  {
      owner = "octopus-code";
      repo = pname;
      rev = "${version}";
      sha256= "1c6q20y0x9aacwa7vp6gj3yvfzain7hnk6skxmvg3wazp02l91kn";
    };

    postPatch = ''
      patchShebangs ./
    '';

    postConfigure = ''
      patchShebangs testsuite/oct-run_testsuite.sh
    '';

    enableParallelBuilding = true;

    preConfigure = with stdenv.lib.strings; ''
      configureFlagsArray+=(
        --with-yaml-prefix=${libyaml}
        --with-fftw-prefix=${fftwJoin}
        --with-gsl-prefix=${gsl}
        --with-libxc-prefix=${libxc}
        --with-netcdf-prefix=${netcdfJoin}
        ${optionalString parallel "--enable-mpi"}
        ${optionalString config.sse2 "--enable-sse2"}
        ${optionalString config.fma "--enable-fma3"}
        ${optionalString config.fma "--enable-fma4"}
        ${optionalString config.avx "--enable-avx"}
        ${optionalString config.avx512 "--enable-avx512"}
        --with-elpa-prefix=${elpa_}
        --with-libvdwxc-prefix=${libvdwxc_}
        --enable-openmp
        CC=mpicc
        CXX=mpicxx
        FC=mpifort
        FCFLAGS_FFTW="-I${fftwJoin}/include"
      )
    '';

    hardeningDisable = [
      "format"
    ];

    meta = with stdenv.lib; {
      description = "Real time quantum dynamics code.";
      license = licenses.lgpl3;
      homepage = "https://www.chemie.uni-bonn.de/pctc/mulliken-center/grimme/software/xtb";
      platforms = platforms.unix;
    };
  }
