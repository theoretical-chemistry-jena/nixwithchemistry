{ stdenv, gfortran, fetchurl
# Dependencies
, glibc
, libX11
, libXmu
, libGL
, libGLU
, makedepend
, which
, vim
}:
stdenv.mkDerivation rec {
    pname = "molden";
    version = "5.9";

    buildInputs = [
      gfortran
      libX11
      libXmu
      libGL
      libGLU
      makedepend
      which
      vim
    ];

    src = fetchurl  {
      url = "ftp://ftp.cmbi.umcn.nl/pub/molgraph/${pname}/${pname}${version}.tar.gz";
      sha256= "152jiw7gv5zxsjjxrnrk9y3yrb5846dbc33nph18z383b8fvpdc8";
    };

    configurePhase = ''
      find -type f -exec sed -i 's!/usr/include!${glibc}/include!g' {} \;
    '';

    patches = [
      ./patches/Makedep.patch
    ];

    makeFlags = [
      "glibc=${glibc}"
      "FC=gfortran"
    ];

    preInstall = ''
      echo "now creating $out/bin"
      mkdir -p $out/bin
    '';

    hardeningDisable = [
      "format"
    ];

    meta = with stdenv.lib; {
      description = "MOLDEN a pre- and post processing program of molecular and electronic structure";
      license = licenses.unfree;
      homepage = "http://cheminf.cmbi.ru.nl/molden/";
      platforms = platforms.unix;
    };
  }
