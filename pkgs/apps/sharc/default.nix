{ stdenv, fetchFromGitHub
# Build time dependencies
, makeWrapper
, gfortran
# Normal link dependencies
, blas
, lapack
, fftw
# Python
, python2
# QC programs
, orca ? null
, bagel
, gaussian ? null
, openmolcas
, turbomole ? null
, wfoverlap
# Plotting
, gnuplot
}:
let
  pythonWP = python2.withPackages (p: with p; [ numpy ]);
in
  stdenv.mkDerivation rec {
    pname = "sharc";
    version = "2.1.1";

    src = fetchFromGitHub  {
      owner = "sharc-md";
      repo = pname;
      rev = "Release${version}";
      sha256= "1p9byiwlyqhbwdq0icxg75n3waxji0fiwp92q8jgrzb384k3bj36";
    };

    nativeBuildInputs = [
      gfortran
      makeWrapper
    ];

    buildInputs = [
      blas
      lapack
      fftw
    ];

    propagatedBuildInputs = with stdenv.lib.lists; [
      pythonWP
      bagel
      openmolcas
    ] ++ optional (orca != null) orca
      ++ optional (gaussian != null) gaussian
      ++ optional (turbomole != null) turbomole
    ;

    hardeningDisable = [
      "format"
    ];

    postPatch = ''
      for binary in $(find ./bin -type f -executable -name "*.x"); do
        rm $binary
      done
      rm bin/sharc.x

      for exe in ./bin/*; do
        patchShebangs $exe
      done
    '';

    /*
    ./source contains the fortran sources and will be compiled into executables, that will be put in
    ./bin then.
    */
    configurePhase = ''
      cd source
    '';

    postBuild = ''
      cd ..
    '';

    installPhase = ''
      $preInstallHook

      mkdir -p $out
      cp -r bin lib $out/.
      cp source/*.x $out/bin/.
      cp ${wfoverlap}/bin/wfoverlap.x $out/bin/wfoverlap_ascii.x

      $postInstallHook
    '';

    binSearchPath = with stdenv.lib.lists; stdenv.lib.strings.makeSearchPath "bin" ([
      openmolcas
      bagel
      gnuplot
    ] ++ optional (orca != null) orca
      ++ optional (gaussian != null) gaussian
      ++ optional (turbomole != null) turbomole
    );

    libSearchPath = with stdenv.lib.lists; stdenv.lib.strings.makeSearchPath "lib" [
      blas
      lapack
      fftw
      stdenv.cc.cc.lib
      gfortran
    ];

    postFixup = ''
      for binary in $(find -type f -executable -name "*.x"); do
        patchelf $binary \
          --set-rpath ${libSearchPath}
      done

      for exe in $(find $out/bin -type f -executable); do
        wrapProgram $exe \
          --prefix PATH : ${binSearchPath} \
          --prefix LD_LIBRARY_PATH : "${libSearchPath}:$out/lib" \
          --set "SHARC" "$out/bin" \
          --set "MOLCAS" ${openmolcas} \
          ${if orca != null then "--set \"ORCADIR\" ${orca}" else ""}
      done
    '';

    meta = with stdenv.lib; {
      description = "The SHARC molecular dynamics (MD) program suite is an ab initio MD software package developed to study the excited-state dynamics of molecules. ";
      license = licenses.gpl3;
      homepage = "https://www.sharc-md.org";
      platforms = platforms.unix;
    };
  }
