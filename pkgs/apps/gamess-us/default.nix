{ stdenv, makeWrapper, fetchFromGitLab, requireFile, gfortran, writeTextFile
# Dependencies
, cmake
, perl
, tcsh
, openmpi
, blas
, hostname
, openssh
}:
assert
  stdenv.lib.asserts.assertMsg
  (stdenv.system == "x86_64-linux")
  "This installation assumes a 64bit linux system.";

assert
  stdenv.lib.asserts.assertMsg
  (builtins.elem blas.passthru.implementation [ "mkl" "openblas" ])
  "The BLAS providers can be either MKL or OpenBLAS.";

assert
  stdenv.lib.asserts.assertMsg
  (!blas.isILP64)
  "A 32 bit integer implementation of BLAS is required.";

let
  blasImplementation = blas.passthru.implementation;
  blasProvider = blas.passthru.provider;

  gamessVersion = "2020R1";
  gfortranVersion = stdenv.lib.versions.majorMinor gfortran.version;
  libxcSource = fetchFromGitLab {
    owner = "libxc";
    repo = "libxc";
    rev = "5.0.0";
    sha256 = "0jdc51dih3v8c41kxdkn3lkcfrjb8qwx36a09ryzik148ra1043p";
  };

  /*
  1.    Starting -> <return>
  2.    Select the target architecture (arbitrary 64 bit linux, also PowerPC)-> linux64
  3.    Select location of the gamess directory. Choose source location for the moment -> <return>
  4.    Select gamess build directory. Build directly here -> <return>
  5.    Give a version string -> ${gammesVersion}
  6.    Select the Fortran compiler -> gfortran
  7.    Give the version of gfortran (first two digits only) -> ${stdenv.lib.versions.majorMinor gfortran.version}
  8.    Continue to math setup -> <return>
  9.    Select the BLAS library to use. -> ${blasImplementation}
  10.   Give the path to the blas library -> ${blasProvider}
  10A.  If MKL was selected, the nix directory structure confuses Gamess for a moment. Long story
        short: directly say proceed in a next line -> "${blasProvider}\nproceed"
  11.  Compile source code activator -> <return>
  12.  Done with source code activator, go to network stuff -> <return>
  13.  Select parallelisation type -> mpi
  14.  Select OpenMPI -> ${openmpi.pname}
  15.  Give the location of the MPI installation. -> ${mpi}
  16.  Build external LibXC -> yes
  17.  Skip reminder for LibXC download -> <return>
  18.  Enables CC active space methods -> yes
  19.  Enable LIBCCHEM -> no
  20.  Enable OpenMP support -> yes
  */
  configAnswers = writeTextFile {
    name = "GAMESS-US_Config_Answers.txt";
    text = ''

    linux64


    ${gamessVersion}
    gfortran
    ${stdenv.lib.versions.majorMinor gfortran.version}

    ${blasImplementation}
    ${if blasImplementation == "mkl" then "${blasProvider}\nproceed" else "${blasProvider}/lib"}


    mpi
    ${openmpi.pname}
    ${openmpi}
    yes

    yes
    no
    yes
    '';
  };

  # Meta information
  pname = "gamess-us";
  version = gamessVersion;
  description = "GAMESS is a program for ab initio molecular quantum chemistry.";
  homepage = "https://www.msg.chem.iastate.edu/gamess/index.html";

  pkgConfig = writeTextFile {
    name = "${pname}.pc";
    text = ''
      prefix=@out@
      exec_prefix=''${prefix}

      Name: ${pname}
      Description: ${description}
      Version: ${version}

      Requires:
      Libs:
      Cflags:
      URL: ${homepage}
    '';
  };

in
  stdenv.mkDerivation rec {
    inherit pname version;


    nativeBuildInputs = [
      makeWrapper
      cmake
      perl
    ];

    buildInputs = [
      gfortran
      blas
    ];

    propagatedBuildInputs = [
      tcsh
      openmpi
    ];

    src = requireFile rec {
      name = "gamess-current.tar.gz";
      sha256 = "94678e567f681d3a7500a1bea68cfb893520e76dcadd7e188eb1e1d185bea90f";
      url = "https://www.msg.chem.iastate.edu/gamess/download.html";
      message = ''
        The GAMESS-US source code is not publically available but follows an easy license agreement.
        Obtain your own license and copy at ${url} and follow the instructions. Download the source
        code version. When you have obtained the current archive, add it to the nix store by:
          nix-store --add-fixed sha256 ${name}
        and then rebuild.
      '';
    };

    # cmake must present to build libxc but should not trigger the actual configuration phase.
    dontUseCmakeConfigure = true;
    enableParallelBuilding = false;

    /*
    Initialise the LibXC source code.
    */
    postUnpack = ''
      mkdir -p $sourceRoot/libxc
      cp -r ${libxcSource}/* $sourceRoot/libxc/.
    '';

    patches = [
      ./patches/MKLLink.patch
      ./patches/rungms.patch
      ./patches/AuxDataPath.patch
    ];

    /*
    Environment variable required by GAMESS to set correct MPI version in rungms script. Replaced at
    build time.
    */
    mpiname = openmpi.pname;
    mpiroot = builtins.toString openmpi;

    /*
    Change all scripts to use tcsh instead of csh
    */
    postPatch = ''
      # Patch the shebangs of all scripts
      cScripts="comp compall config gms-files.csh lked runall rungms rungms-dev ddi/compddi"
      for s in $cScripts; do
        substituteInPlace $s \
          --replace "/bin/csh" "/bin/tcsh" \
          --replace "/usr/bin/env csh" "/bin/tcsh"
        patchShebangs $s
      done
      patchShebangs tools

      # Increase the maximum numbers of CPUs per node and maximum number of Nodes to a more reasonable
      # value for DDI
      substituteInPlace ddi/compddi \
        --replace "set MAXCPUS=32" "set MAXCPUS=256"

      # Prepare the rungms script
      # Replace references to @version@ and @out@
      substituteAllInPlace rungms

      # Make config accept dynamic OpenBLAS
      substituteInPlace config \
        --replace "libopenblas.a" "libopenblas.so"
    '';

    configurePhase = ''
      ./config < ${configAnswers}
      if [ "${blasImplementation}" == "mkl" ]; then
        substituteInPlace install.info \
          --replace "setenv GMS_MATHLIB_PATH      ${blasProvider}/lib/intel64" "setenv GMS_MATHLIB_PATH      ${blasProvider}/lib"
      fi
    '';

    buildFlags = [
      "libxc"
      "gamess"
    ];

    /*
    Of course also the installation is quite custom ... Take care of the installation.
    */
    installPhase = ''
      runHook preInstall


      mkdir -p $out/bin $out/share $out/share/gamess

      # Copy the interesting scripts and executables
      cp gamess.${gamessVersion}.x rungms $out/bin/.

      # Copy the file definitons stuff to share
      cp gms-files.csh $out/share/gamess

      # Copy auxdata, which contains parameters and basis sets
      cp -r auxdata $out/share/gamess/.

      # Copy the test files
      cp -r tests $out/share/gamess/.


      runHook postInstall
    '';

    postInstall = ''
      mkdir -p $out/lib/pkgconfig

      substitute ${pkgConfig} $out/lib/pkgconfig/${pkgConfig.name} \
        --subst-var "out"
    '';

    binSearchPath = stdenv.lib.strings.makeSearchPath "bin" [ openssh openmpi tcsh hostname ];
    /*
    Patch the entry point to fit this systems needs for running an actual calculation.
    */
    postFixup = ''
       wrapProgram $out/bin/rungms \
         --set-default SCRATCH "/tmp" \
         --prefix PATH : ${binSearchPath}
    '';

    hardeningDisable = [
      "format"
    ];

    meta = with stdenv.lib; {
      inherit description homepage;
      license = licenses.unfree;
      platforms = platforms.unix;
    };
  }
