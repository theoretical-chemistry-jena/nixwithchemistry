{ fetchFromGitLab, buildPythonPackage, lib, writeTextFile, makeWrapper, fetchurl
, blas
, lapack
, scalapack
, mpi
, fftw
, libxc
, libvdwxc
, which
# Python dependencies
, ase
, numpy
, scipy
}:
assert
  lib.asserts.assertMsg
  (!blas.isILP64)
  "A 32 bit integer implementation of BLAS is required.";

assert
  lib.asserts.assertMsg
  (!lapack.isILP64)
  "A 32 bit integer implementation of LAPACK is required.";

let
  libvdwxc_ = libvdwxc.override {
    inherit mpi;
    fftw = fftw_;
  };

  fftw_ = fftw.override {
    inherit mpi;
  };

  scalapack_ = scalapack.override {
    inherit blas lapack mpi;
  };

  gpawConfig = writeTextFile {
    name = "siteconfig.py";
    text = ''
      # Compiler
      compiler = 'gcc'
      mpicompiler = '${mpi}/bin/mpicc'
      mpilinker = '${mpi}/bin/mpicc'

      # BLAS
      libraries += ['blas']
      library_dirs += ['${blas}/lib']

      # FFTW
      fftw = True
      if fftw:
        libraries += ['fftw3']

      scalapack = True
      if scalapack:
        libraries += ['scalapack']

      # LibXC
      libxc = True
      if libxc:
        xc = '${libxc}/'
        include_dirs += [xc + 'include']
        library_dirs += [xc + 'lib/']
        extra_link_args += ['-Wl,-rpath={xc}/lib'.format(xc=xc)]
        if 'xc' not in libraries:
          libraries.append('xc')

      # LibVDWXC
      libvdwxc = True
      if libvdwxc:
        vdwxc = '${libvdwxc_}/'
        extra_link_args += ['-Wl,-rpath=%s/lib' % vdwxc]
        library_dirs += ['%s/lib' % vdwxc]
        include_dirs += ['%s/include' % vdwxc]
        libraries += ['vdwxc']
    '';
  };

  setupVersion = "0.9.20000";
  pawDataSets = fetchurl {
    url = "https://wiki.fysik.dtu.dk/gpaw-files/gpaw-setups-${setupVersion}.tar.gz";
    sha256 = "07yldxnn38gky39fxyv3rfzag9p4lb0xfpzn15wy2h9aw4mnhwbc";
  };
in
  buildPythonPackage rec {
    pname = "gpaw";
    version = "20.1.0";

    nativeBuildInputs = [
      which
      makeWrapper
    ];

    buildInputs = [
      blas
      scalapack_
      fftw_
      libxc
      libvdwxc_
    ];

    propagatedBuildInputs = [
      ase
      scipy
      numpy
      mpi
    ];

    src = fetchFromGitLab  {
      owner = "gpaw";
      repo = pname;
      rev = version;
      sha256= "1gk0510hj9ijssa0kfivgzmwsdlaqd01p0x8zd9jy7sl0wqwwzhc";
    };

    preConfigure = ''
      export PATH=$PATH:${mpi}/bin
    '';

    postInstall = ''
      currDir=$(pwd)
      mkdir -p $out/share/gpaw && cd $out/share/gpaw
      cp ${pawDataSets} gpaw-setups.tar.gz
      tar -xvf $out/share/gpaw/gpaw-setups.tar.gz
      rm gpaw-setups.tar.gz
      cd $currDir
    '';

    doCheck = false;
    preCheck = ''
      # export PATH=$PATH:${mpi}/bin
      export GPAW_SETUP_PATH=$out/share/gpaw/gpaw-setups-${setupVersion}
      pwd
    '';

    postPatch = ''
      cp ${gpawConfig} siteconfig.py
    '';

    postFixup = ''
      for exe in $out/bin/*; do
        wrapProgram $exe \
          --set "GPAW_SETUP_PATH" "$out/share/gpaw/gpaw-setups-${setupVersion}"
      done
      ln -s ${mpi}/bin/mpiexec $out/bin/.
      ln -s ${mpi}/bin/mpirun $out/bin/.
    '';

    meta = with lib; {
      description = "DFT and beyond within the projector-augmented wave method";
      license = licenses.gpl3;
      homepage = "https://wiki.fysik.dtu.dk/gpaw/index.html";
      platforms = platforms.unix;
    };
  }
