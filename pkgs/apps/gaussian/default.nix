{ stdenv, makeWrapper, requireFile, nix-info, config, writeScriptBin, rsync, coreutils, writeTextFile
, tcsh
, bash
}:
assert stdenv.lib.asserts.assertMsg config.avx2 "Gaussian requires the AVX2 CPU extensions, which are not available";
assert stdenv.lib.asserts.assertMsg (stdenv.system == "x86_64-linux") "Gaussian only runs on x86_64 linux systems";

let
  unpackedDirName = "g16";

  /*
  Entry points one could/would call to invoke Gaussian 16. Others Gaussian executables are called by
  those entry points. Entry points will be treated differently by the wrapper.
  */
  entryPoints = [
    "g16"
    "formchk"
    "unfchk"
    "freqchk"
    "cubegen"
    "trajgen"
  ];

  /*
  This is a replacement for Gaussian executables, that are entry points. Those scripts are not meant
  to live in the Gaussian installation directory ($out/${unpackedDirName}), but in $out/bin.
  These entry point replacements actually create a temporary Gaussian installation directory in the
  local directory from $out/${unpackedDirName} and provide environment variables.

  This script needs to be modified during building the derivation, by replacing the following
  variables:
    - @out@ -> Derivation prefix $out
  */
  entryPointReplacement = writeScriptBin "entryPointReplacement.sh" ''
    #!${bash}/bin/bash
    set +x
    set -e

    # Define a cleanup function after crashes or normal termination of Gaussian.
    function cleanup {
      rm -rf $LOCAL_GAUSS
      export PATH=$originalPATH
      umask $originalUmask
      unset NIX_GAUSS
      unset LOCAL_GAUSS
      unset g16root
      unset GAUSS_EXEDIR
      unset GAUSS_SCRDIR
    }

    # Create a trap in case something goes wrong.
    trap cleanup EXIT ERR

    # Get the original PATH and Umask variable before modifications.
    originalPATH=$PATH
    originalUmask=$(umask)

    # Set umask for correct permission of all files.
    umask 0023

    # The original installation directory of Gaussian in the nix store.
    export NIX_GAUSS=@out@/${unpackedDirName}

    # The new installation directory of Gaussian in a local directory.
    export LOCAL_GAUSS=$(pwd)/${unpackedDirName}

    # Paths of all Gaussian executables relative to @out@/g16.
    GAUSS_EXES=$(find $NIX_GAUSS -type f -executable ! -name ".*-wrapped" | sed "s:$NIX_GAUSS/::")

    # Creation of the local installation prefix.
    mkdir -p $LOCAL_GAUSS

    # Put the subdirectories into the local prefix.
    cp -r --no-preserve=all $NIX_GAUSS/basis $NIX_GAUSS/linda* $NIX_GAUSS/bsd $NIX_GAUSS/tests $NIX_GAUSS/doc $LOCAL_GAUSS/.

    # Set permissions for the local Gaussian installation directory.
    chown -R $USER $LOCAL_GAUSS
    chmod -R go-rwx $LOCAL_GAUSS
    chmod -R u+rx $LOCAL_GAUSS

    # Copy all the wrapper scripts to this directory, which shall on demand get the executables from
    # the nix store.
    for e in $GAUSS_EXES; do
      cp $NIX_GAUSS/$e $LOCAL_GAUSS/$e
    done

    # Copy all the parameter and other files in the installation directory.
    for f in $(find $NIX_GAUSS -name "*.hlp" -o -name "*.prm") $(find $NIX_GAUSS -maxdepth 1 -type l); do
      cp $f $LOCAL_GAUSS/.
    done

    # Set environment variables to execute Gaussian.
    export g16root=$(dirname $LOCAL_GAUSS) # Removes also g16 (as it thinks this is the name of a file)
    export GAUSS_EXEDIR=$LOCAL_GAUSS
    export GAUSS_SCRDIR=/tmp
    export PATH=$LOCAL_GAUSS:$PATH

    # Get the name of this script, which is also the name of an entry point executable and then call
    # it from the local installation prefix.
    ENTRY_EXE_NAME=$(basename $0)
    $LOCAL_GAUSS/$ENTRY_EXE_NAME "$@"
  '';

  /*
  This wrapper replaces all executables by this wrapper. The wrapper is then copied to a local
  installation prefix of Gaussian. Afterwards when being called, this wrapper fetches the actual
  binary from the store to the local prefix and sets correct permissions.
  */
  genericPermissionWrapper = writeScriptBin "genericPermissionWrapper.sh" ''
    #!${bash}/bin/bash
    set +x
    set -e

    # Define a cleanup function after crashes or normal termination of Gaussian.
    function cleanup {
      unset NIX_GAUSS
      unset LOCAL_GAUSS
      unset g16root
      unset GAUSS_EXEDIR
      unset GAUSS_SCRDIR
    }

    # Create a trap in case something goes wrong.
    trap cleanup EXIT ERR

    # Set environment variables to execute Gaussian.
    export NIX_GAUSS=@out@/${unpackedDirName}
    export LOCAL_GAUSS=$(pwd)/${unpackedDirName}
    export g16root=$(dirname $LOCAL_GAUSS) # Removes also g16 (as it thinks this is the name of a file)
    export GAUSS_EXEDIR=$LOCAL_GAUSS
    export GAUSS_SCRDIR=/tmp

    # Get the path of this executable relative to the local installation prefix.
    EXE_REL_PATH=$(realpath $0 | sed "s:$LOCAL_GAUSS/::")
    #EXE_WRAP_REL_PATH=.$\{EXE_REL_PATH\}-wrapped
    EXE_WRAP_REL_PATH=.$(echo $EXE_REL_PATH)-wrapped

    # The the corresponding executable from the Nix installation prefix.
    ${rsync}/bin/rsync $NIX_GAUSS/$EXE_WRAP_REL_PATH $LOCAL_GAUSS/$EXE_WRAP_REL_PATH

    # Modify the permissions of the just copied binary.
    chown $USER $LOCAL_GAUSS/$EXE_WRAP_REL_PATH
    chmod -R go-rwx $LOCAL_GAUSS

    # Execute the binary now with all arguments possibly passed to it.
    $LOCAL_GAUSS/$EXE_WRAP_REL_PATH "$@"
  '';

  # Meta information
  pname = "gaussian";
  version = "16B01";
  description = "General purpose quantum chemistry program";
  homepage = "http://gaussian.com/";

  pkgConfig = writeTextFile {
    name = "${pname}.pc";
    text = ''
      prefix=@out@
      exec_prefix=''${prefix}

      Name: ${pname}
      Description: ${description}
      Version: ${version}

      Requires:
      Libs:
      Cflags:
      URL: ${homepage}
    '';
  };

in
  stdenv.mkDerivation rec {
    inherit pname version;

    nativeBuildInputs = [
      makeWrapper
      nix-info
    ];

    propagatedBuildInputs = [
      tcsh
      bash
    ];

    src =
      let
        fileName = "G16.B.01.tbJ";
      in
        requireFile {
          name = fileName;
          sha256 = "c3326ac9167357d47e0affc09f9b84c05d0581127d179e47ebd781c510ef9995";
          message = ''
            The Gaussian binaries need to be obtained from disk. You will need the AVX2 enabled binary
            archive ${fileName}. Add it to the nix-store by:
              nix-store --add-fixed sha256 ${fileName}
            and then rebuild.
          '';
        };

    unpackPhase = ''
      tar -xvf $src
      cd ${unpackedDirName}
    '';

    patches = [
      ./patches/RelativeRmPath.patch
    ];
    postPatch = ''
      bsdFiles=$(find bsd/ -type f ! -name "*.exe" ! -name "*.log" ! -name "*.cube")
      cshFiles=$(find -name "*.csh")
      for f in $bsdFiles $cshFiles; do
        substituteInPlace $f --replace '#!/bin/csh' '#!${tcsh}/bin/tcsh'
      done
    '';

    dontConfigure = true;
    dontBuild = true;

    installPhase = ''
      runHook preInstall

      tcsh ./bsd/install
      cd ..
      mkdir -p $out $out/bin
      cp -r ${unpackedDirName} $out/.

      runHook postInstall
    '';

    postInstall = ''
    mkdir -p $out/lib/pkgconfig

    substitute ${pkgConfig} $out/lib/pkgconfig/${pkgConfig.name} \
    --subst-var "out"
    '';

    fixupPhase = ''
      echo "fixupPhase"
      export g16root=$out
      echo "Set g16root to: $g16root"

      # Fixing LINDA_PATH in linda shell scripts
      echo "Fixing LINDA_PATH in linda scripts (again)"
      cd $g16root/${unpackedDirName}/linda9.1/linux64bit/bin
      lindaScripts=$(find -type f)
      originalLindaPath=$(grep "LINDA_PATH=" vntsnet | cut -c 12-)
      newLindaPath=$g16root/${unpackedDirName}/linda9.1/linux64bit/
      echo "Original LINDA_PATH: $originalLindaPath"
      echo "Correct (new) LINDA_PATH: $newLindaPath"
      for l in $lindaScripts; do
        substituteInPlace $l --replace "LINDA_PATH=$originalLindaPath" "LINDA_PATH=$newLindaPath"
      done
      echo "Done with fixing LINDA_PATH"


      # Apply the wrapper script to all executables.
      for exe in $(find $g16root/${unpackedDirName}/ -type f -executable); do

        # Move the original binary to a new name.
        mv $exe $(dirname $exe)/.$(basename $exe)-wrapped

        # Replace the original executable by its wrapper script and set placeholders correctly.
        substitute ${genericPermissionWrapper}/bin/genericPermissionWrapper.sh $exe \
          --subst-var out

        # Make the wrapper executable.
        chmod +x $exe

      done


      # Create an entry point script in bin
      for entryPoint in ${toString entryPoints}; do
        substitute ${entryPointReplacement}/bin/entryPointReplacement.sh $out/bin/$entryPoint \
          --subst-var out
        chmod +x $out/bin/$entryPoint
      done
    '';


    meta = with stdenv.lib; {
      inherit description homepage;
      license = licenses.unfree;
      platforms = platforms.linux;
      architectures = [ "amd64" ];
    };
  }
