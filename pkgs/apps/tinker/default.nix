{ stdenv, gfortran, cmake, writeTextFile
, fftw
, pkgconf
}:
let
  pname = "tinker";
  version = "8.8.1";
  description = "Software Tools for Molecular Design";
  homepage = "https://dasher.wustl.edu/tinker/";

  pkgConfig = writeTextFile {
    name = "${pname}.pc";
    text = ''
      prefix=@out@
      exec_prefix=''${prefix}
      libdir=''${exec_prefix}/lib
      sharedlibdir=''${libdir}
      includedir=''${prefix}/include

      Name: ${pname}
      Description: ${description}
      Version: ${version}

      Requires:
      Libs:
      Cflags:
      URL: ${homepage}
    '';
  };
in
  stdenv.mkDerivation rec {
    inherit pname version;


    src = fetchTarball  {
      url = "https://dasher.wustl.edu/tinker/downloads/tinker-${version}.tar.gz";
      sha256= "05almjgd2hpz5wqdnlm835pw3lkl0zw09yv8dx7f929a9aq7rgg2";
    };

    preConfigure = ''
      cd source
      cp ../cmake/CMakeLists.txt .
    '';

    nativeBuildInputs = [
      cmake
      gfortran
      pkgconf
    ];

    buildInputs = [
      fftw
    ];

    postInstall = ''
      mkdir -p $out/share/tinker
      cp -r ../../params $out/share/tinker

      for exe in $(find $out/bin/ -type f -executable -name "*.x"); do
        ln -s $exe $out/bin/$(basename $exe .x)
      done

      mkdir -p $out/lib/pkgconfig
      substitute ${pkgConfig} $out/lib/pkgconfig/${pkgConfig.name} \
        --subst-var "out"
    '';

    meta = with stdenv.lib; {
      inherit description homepage;
      license = licenses.unfree;
      platforms = platforms.unix;
    };
  }
