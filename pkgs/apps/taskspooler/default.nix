{ stdenv, makeWrapper
, coreutils
}:
stdenv.mkDerivation rec {
    pname = "taskspooler";
    version = "1.0";

    /*
    src = builtins.fetchTarball {
      url = "https://vicerveza.homeunix.net/%7Eviric/wsgi-bin/hgweb.wsgi/ts/archive/a415e55ad9bd.tar.gz";
      sha256= "1q0hypbns6zzklivpyvzqkwbixqaq9aqkm2qlqjvm0p3rc0y857g";
    };
    */

    src = builtins.fetchurl {
      url = "http://deb.debian.org/debian/pool/main/t/task-spooler/task-spooler_1.0.orig.tar.gz";
      sha256 = "4f53e34fff0bb24caaa44cdf7598fd02f3e5fa7cacaea43fa0d081d03ffbb395";
    };

    /*
    src = builtins.path {
      path = ./task-spooler_1.0.orig.tar.gz;
    };
    */

    nativeBuildInputs = [
      makeWrapper
    ];

    configurePhase = ''
      substituteInPlace Makefile \
        --replace "PREFIX?=/usr/local" "PREFIX=$out"
    '';

    postFixup = ''
      wrapProgram $out/bin/ts \
        --set-default TS_SLOTS "$(${coreutils}/bin/nproc --all)"
    '';


    meta = with stdenv.lib; {
      description = "Simple single node task scheduler";
      license = licenses.gpl2;
      homepage = "https://vicerveza.homeunix.net/~viric/wsgi-bin/hgweb.wsgi/ts";
      platforms = platforms.unix;
    };
  }
