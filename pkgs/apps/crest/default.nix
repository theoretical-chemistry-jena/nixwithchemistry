{ stdenv, fetchurl, makeWrapper, cmake, gfortran, blas, fetchFromGitHub, autoPatchelfHook
# Dependencies
, xtb
# Configuration
, useBin ? true
}:
stdenv.mkDerivation rec {
  pname = "crest";
  version = "2.11";

  nativeBuildInputs = [
    cmake
    makeWrapper
    gfortran
  ] ++ stdenv.lib.lists.optional useBin autoPatchelfHook
  ;

  buildInputs = [
    blas
  ];

  propagatedBuildInputs = [
    xtb
  ];

  src = if useBin
    then builtins.fetchurl {
      url = "https://github.com/grimme-lab/${pname}/releases/download/v${version}/${pname}.tgz";
      sha256 = "1r26w3mi91q71dxfwgbjkd2jvj07s8jc9r3iz18wj1zmsda4c35a";
    } else fetchFromGitHub {
      repo = pname;
      owner = "grimme-lab";
      rev = "2674ba27bf7a5f3c20c6f505cdf0f8a852eda1d5";
      sha256 = "1lfgyldl2qalab34v1isz07gwzxlb4cy919xv2lfa5zp46g35r05";
    };

  FFLAGS = "-ffree-line-length-512";

  unpackPhase = ''
    tar -xvf $src
  '';


  dontConfigure = useBin;
  dontBuild = useBin;

  installPhase = if useBin
    then ''
      runHook preInstall
      mkdir -p $out/bin
      cp crest $out/bin/.
      runHook postInstall
    '' else ''
      runHook preInstall
      make install
      runHook postInstall
    '';


  hardeningDisable = [
    "all"
  ];

  postFixup = ''
    wrapProgram $out/bin/crest \
      --prefix PATH : "${xtb}/bin"
  '';

  meta = with stdenv.lib; {
    description = "Conformer-Rotamer Ensemble Sampling Tool based on the xtb Semiempirical Extended Tight-Binding Program Package";
    license = licenses.gpl3;
    homepage = "https://github.com/grimme-lab/crest";
    platforms = [ "x86_64-linux" ] ;
  };
}
