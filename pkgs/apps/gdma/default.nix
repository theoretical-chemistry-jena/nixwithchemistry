{ stdenv, gfortran, fetchFromGitLab, python2, writeTextFile }:
let
  pname = "gdma";
  version = "2.3.3";
  description = "Global Distributed Multipole Analysis from Gaussian Wavefunctions";
  homepage = "http://www-stone.ch.cam.ac.uk/pub/gdma/";

  pkgConfig = writeTextFile {
    name = "${pname}.pc";
    text = ''
      prefix=@out@
      exec_prefix=''${prefix}

      Name: ${pname}
      Description: ${description}
      Version: ${version}

      Requires:
      Libs:
      Cflags:
      URL: ${homepage}
    '';
  };

in
  stdenv.mkDerivation rec {
    inherit pname version;

    depsBuildBuild = [
      gfortran
    ];

    buildInputs = [
      python2
    ];

    src = fetchFromGitLab  {
      owner = "anthonyjs";
      repo = pname;
      rev = "ce9c35bd09876698f55548386d93b0fae2ef2702";
      sha256= "0i67fnchx4gjh8ilim4qq9q5nh7jk2d8252bicjlibwfq7jqzbay";
    };

    patches = [
      ./patches/PythonVersionpy.patch
    ];

    installPhase = ''
      runHook preInstall

      mkdir -p $out/bin
      cp -p bin/gdma $out/bin

      runHook postInstall
    '';

    postInstall = ''
      mkdir -p $out/lib/pkgconfig

      substitute ${pkgConfig} $out/lib/pkgconfig/${pkgConfig.name} \
        --subst-var "out"
    '';

    hardeningDisable = ["format"];

    meta = with stdenv.lib; {
      inherit description homepage;
      license = licenses.gpl3;
      platforms = platforms.unix;
    };
  }
