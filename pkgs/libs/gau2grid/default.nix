{ stdenv, fetchFromGitHub, cmake
, python
, numpy
, setuptools
# Configuration options
, maxAm ? 7
}:
stdenv.mkDerivation rec {
    pname = "gau2grid";
    version = "2.0.4";

    nativeBuildInputs = [
      cmake
    ];

    propagatedBuildInputs = [
     python
     numpy
     setuptools
    ];

    cmakeFlags = [
     "-DMAX_AM=${toString maxAm}"
    ];

    src = fetchFromGitHub  {
      owner = "dgasmith";
      repo = pname;
      rev = "v" + version;
      sha256= "0qypq8iax0n6yfi4223zya468v24b60nr0x43ypmsafj0104zqa6";
    };

    meta = with stdenv.lib; {
      description = "Fast computation of a gaussian and its derivative on a grid";
      homepage = "https://github.com/dgasmith/gau2grid";
      license = licenses.bsd3;
      platforms = platforms.all;
    };
  }
