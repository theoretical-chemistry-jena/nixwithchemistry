{ stdenv, fetchFromGitHub, cmake, lib
, blas
# Python
, python
, numpy
}:
stdenv.mkDerivation rec {
  pname = "libcint";
  version = "3.0.20";

  nativeBuildInputs = [
    cmake
  ];

  buildInputs = [
    blas
  ];

  propagatedBuildInputs = [
    python
    numpy
  ];

  # somehow gets /nix/store/.. twice. Therefore set install directory to "/".
  cmakeFlags = [
    "-DCMAKE_INSTALL_PREFIX:PATH=/"
    "-DENABLE_TEST=0"
    "-DWITH_RANGE_COULOMB=1"
    "-DWITH_F12=1"
    "-DWITH_COULOMB_ERF=1"
  ];

  src = fetchFromGitHub  {
    owner = "sunqm";
    repo = "libcint";
    rev = "v${version}";
    sha256= "0iqqq568q9sxppr08rvmpyjq0n82pm04x9rxhh3mf20x1ds7ngj5";
  };

  meta = with stdenv.lib; {
    description = "General GTO integrals for quantum chemistry ";
    homepage = "https://github.com/sunqm/libcint";
    license = licenses.bsd2;
    platforms = platforms.unix;
  };
}
