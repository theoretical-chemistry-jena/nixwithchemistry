{ stdenv, fetchFromGitHub, fetchpatch
# Build dependencies
, autoconf
, automake
, perl
, libtool
, python3
# Link dependencies
, boost
, gmpxx
, mpfr
, eigen
, gfortran
# Settings
, config
, enableEriDeriv ? 1
, maxAm ? 7
, optAm ? 6
, shellSet ? "standard"
, cartgaussOrder ? "standard"
}:
assert
  stdenv.lib.asserts.assertMsg
  (builtins.elem shellSet [ "standard" "orca" ])
  "Invalid shellSet option specified.";

assert
  stdenv.lib.asserts.assertMsg
  (builtins.elem cartgaussOrder [ "standard" "intv3" "gamess" "orca" "bagel" ])
  "Invalid cartgaussOrder option specified.";


stdenv.mkDerivation rec {
  pname = "libint";
  version = "2.6.0";

  src = fetchFromGitHub  {
    owner = "evaleev";
    repo = pname;
    rev = "v${version}";
    sha256= "0pbc2j928jyffhdp4x5bkw68mqmx610qqhnb223vdzr0n2yj5y19";
  };

  nativeBuildInputs = [
    autoconf
    automake
    libtool
    mpfr
    perl
    python3
  ];

  buildInputs = [
    boost
    gmpxx
    gfortran
  ];

  enableParallelBuilding = true;

  patches = [
    /* 404 error on debian server
    (fetchpatch {
      name = "fortran_bindings";
      url = "https://sources.debian.org/data/main/libi/libint2/2.6.0-2/debian/patches/fortran_bindings.patch";
      sha256 = "0x71xldmk0agdk61x7k39r743nvq3irxy6s3djyg59r8yby9a6vc";
    */
    ./patches/fortran_bindings_debian.patch # Original from https://sources.debian.org/patches/libint2/2.6.0-4/fortran_bindings.patch/
  ];

  postPatch = ''
    find -name Makefile -exec sed -i 's:/bin/rm:rm:' \{} \;
  '';

  preConfigure = ''
    ./autogen.sh

    configureFlagsArray+=(
      --enable-shared=yes
      --enable-static=yes
      --enable-generic-code
      --enable-contracted-ints
      --with-shell-set=${shellSet}
      --with-cartgauss-ordering=${cartgaussOrder}
      --enable-eri=${toString enableEriDeriv}
      --enable-eri2=${toString enableEriDeriv}
      --enable-eri3=${toString enableEriDeriv}
      --with-max-am=${toString maxAm}
      --with-eri-max-am=${toString maxAm},${toString (maxAm - 1)}
      --with-eri2-max-am=${toString (maxAm + 2)},${toString (maxAm + 1)}
      --with-eri3-max-am=${toString (maxAm + 2)},${toString (maxAm + 1)}
      --with-opt-am=${toString optAm}
      ${if config.fma then "--enable-fma" else ""}
    )
  '';

  /*
  Fix something with the Fortran interface, whatever ... Taken from markuskowa:
  https://github.com/markuskowa/NixOS-QChem/blob/3627a7277eb9c48b4cf2cba3d57feead9a695dcd/libint/default.nix#L50
  */
  postBuild = ''
    cd export/fortran
    make libint_f.o ENABLE_FORTRAN=yes
    cd ../..
  '';

  postInstall = ''
    cp export/fortran/libint_f.mod $out/include/
  '';

  passthru = {
    inherit maxAm optAm shellSet cartgaussOrder;
  };

  meta = with stdenv.lib; {
    description = "High-performance library for computing Gaussian integrals in quantum mechanics";
    license = licenses.gpl3;
    homepage = "https://github.com/evaleev/libint/tree/v2.6.0";
    platforms = platforms.unix;
  };
}
