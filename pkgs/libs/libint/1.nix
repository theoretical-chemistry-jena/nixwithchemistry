{ stdenv, fetchFromGitHub
# Build dependencies
, cmake
# Settings
, maxAmEri ? 8
}:
stdenv.mkDerivation rec {
  pname = "libint";
  version = "1.2.1";

  src = fetchFromGitHub  {
    owner = "evaleev";
    repo = pname;
    rev = "release-1-2-1";
    sha256 = "1kk4l4fwfxli8p60cdg37n5fhgg14a6591nqhm0h1qk1xq4vyak9";
  };

  nativeBuildInputs = [
    cmake
  ];

  enableParallelBuilding = true;

  cmakeFlags = [
    "-DBUILD_FPIC=ON"
    "-DENABLE_XHOST=OFF"
    "-DMAX_AM_ERI=${toString maxAmEri}"
    "-DBUILD_SHARED_LIBS=ON"
    "-DCMAKE_BUILD_TYPE=RELEASE"
    "-DENABLE_GENERIC=ON"
    "-DMERGE_LIBDERIV_INCLUDEDIR=OFF"
    "-DCMAKE_INSTALL_PREFIX=$out"
  ];

  configurePhase = ''
    cmake -Bbuild ${toString cmakeFlags}
    cd build
  '';

  # Somehow as if -DMERGE_LIBDERIV_INCLUDEDIR=ON, but does not remove the header file from its own diretory.
  postInstall = ''
    cp $out/include/libderiv/libderiv.h $out/include/libint/.
  '';

  passthru = {
    inherit maxAmEri;
  };

  meta = with stdenv.lib; {
    description = "High-performance library for computing Gaussian integrals in quantum mechanics";
    license = licenses.gpl3;
    homepage = "https://github.com/evaleev/libint";
    platforms = platforms.unix;
  };
}
