{ stdenv, fetchFromGitHub, cmake, gfortran
# Python
, python
, pybind11
}:

stdenv.mkDerivation rec {
  pname = "xcfun";
  version = "2.0.2";

  nativeBuildInputs = [
    cmake
    gfortran
  ];

  propagatedBuildInputs = [
    python
    pybind11
  ];

  /*
  Something goes wrong when using normal configurePhase and includes become wrong.
  Whatever this changes here ...
  */
  configurePhase = ''
    cmake -Bbuild \
      -DBUILD_SHARED_LIBS=ON \
      -DXCFUN_MAX_ORDER=3 \
      -DCMAKE_BUILD_TYPE=RELEASE \
      -DXCFUN_ENABLE_TESTS=0 \
      -DCMAKE_INSTALL_PREFIX=$out
    cd build
  '';

  enableParallelBuilding = true;

  src = fetchFromGitHub  {
    owner = "dftlibs";
    repo = pname;
    rev = "v${version}";
    sha256= "0c989jncalswaqh44d1hb8zqm38h6bjaii6v57zy282nb4qc8hf8";
  };

  meta = with stdenv.lib; {
    description = "A library of exchange-correlation functionals with arbitrary-order derivatives.";
    homepage = "https://github.com/dftlibs/xcfun";
    license = licenses.mpl20;
    platforms = platforms.unix;
  };
}
