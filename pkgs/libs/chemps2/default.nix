{ stdenv, fetchFromGitHub, cmake
, blas
, lapack
, hdf5
, hdf5-cpp
, mpi ? null
}:
assert
  stdenv.lib.asserts.assertMsg
  (!blas.isILP64)
  "32 bit integer BLAS implementation required.";

stdenv.mkDerivation rec {
  pname = "CheMPS2";
  version = "1.8.9";

  src = fetchFromGitHub {
    owner = "SebWouters";
    repo = "CheMPS2";
    rev = "v${version}";
    sha256 = "0813z3myyri11lhh18kfpg5xs7imds9dg4kmab82lpp2isymakic";
  };

  nativeBuildInputs = [
    cmake
  ];

  buildInputs = [
    blas
    lapack
    hdf5
    hdf5-cpp
  ];

  preConfigure = stdenv.lib.strings.optionalString (mpi != null) ''
    export CXX=${mpi}/bin/mpicxx
  '';

  cmakeFlags = [
    "-DCMAKE_RANLIB=${stdenv.cc.cc}/bin/gcc-ranlib"
    "-DCMAKE_AR=${stdenv.cc.cc}/bin/gcc-ar"
  ]
  ++ stdenv.lib.lists.optional (mpi != null) "-DWITH_MPI=ON"
  ;

  doCheck = (mpi == null);

  preCheck = ''
    export LD_LIBRARY_PATH=$PWD/CheMPS2
    export OMP_NUM_THREADS=1
  '';

  meta = with stdenv.lib; {
    description = "A spin-adapted implementation of DMRG for ab initio quantum chemistry";
    homepage = "https://github.com/SebWouters/CheMPS2";
    license = licenses.gpl2;
    platforms = platforms.all;
  };
}
