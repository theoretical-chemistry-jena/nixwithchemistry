{ stdenv, fetchurl, gfortran, perl
# Global configuration
, config
# Configuration options
, precision ? "double"
, mpi ? null
}:
assert builtins.elem precision [ "single" "double" "long-double" "quad-precision" ];
let
  withDoc = stdenv.cc.isGNU;

in
  stdenv.mkDerivation rec {
    pname = "fftw-${precision}";
    version = "3.3.8";

    buildInputs = stdenv.lib.lists.optional (mpi != null) mpi;

    src = fetchurl {
      urls = [
        "http://fftw.org/fftw-${version}.tar.gz"
        "ftp://ftp.fftw.org/pub/fftw/fftw-${version}.tar.gz"
      ];
      sha256 = "00z3k8fq561wq2khssqg0kallk0504dzlx989x3vvicjdqpjc4v1";
    };

    outputs = [
      "out"
      "dev"
      "man"
    ];

    outputBin = "dev"; # fftw-wisdom

    hardeningDisable = [
      "format"
    ];

    nativeBuildInputs = [
      gfortran
    ];

    configureFlags = with stdenv.lib.lists; [
      "--enable-shared"
      "--enable-static"
      "--enable-openmp"
      "--enable-threads"
      "--disable-doc"
    ]
    ++ optional (mpi != null) "--enable-mpi"
    ++ optional (precision != "double") ("--enable-${precision}")
    ++ optional (config.sse2 && (precision == "single" || precision == "double")) "--enable-sse2"
    ++ optional (config.avx && (precision == "single" || precision == "double")) "--enable-avx"
    ++ optional (config.avx2 && (precision == "single" || precision == "double")) "--enable-avx2"
    ++ optional (config.avx512 && (precision == "single" || precision == "double")) "--enable-avx512"
    ++ optional (mpi != null) "MPICC=${mpi}/bin/mpicc"
    ++ optional (mpi != null) "MPIFC=${mpi}/bin/mpif90"
    ++ optional (mpi != null) "MPIF90=${mpi}/bin/mpif90"
    ;

    enableParallelBuilding = true;

    meta = with stdenv.lib; {
      description = "Fastest Fourier Transform in the West";
      homepage = "http://www.fftw.org/";
      license = licenses.gpl2Plus;
      platforms = platforms.all;
    };
  }
