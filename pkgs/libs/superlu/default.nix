{ stdenv, fetchFromGitHub, gfortran, cmake
, blas
}:
# Works both with 64 and 32 bit BLAS.
stdenv.mkDerivation rec {
    pname = "superlu";
    version = "5.2.1";

    nativeBuildInputs = [
      cmake
    ];

    buildInputs = [
      gfortran
    ];

    propagatedBuildInputs = [
      blas
    ];

    src = fetchFromGitHub {
      owner = "xiaoyeli";
      repo = "superlu";
      rev = "v${version}";
      sha256 = "0y6lpr77qk1xs9ka8c31h5fcpvz4vzna81s4pj9lq584m99p5f7f";
    };

    cmakeFlags = [
      "-DBUILD_SHARED_LIBS=true"
      "-DUSE_XSDK_DEFAULTS=true"
    ];

    hardeningDisable = [
      "format"
    ];

    doCheck = true;

    meta = with stdenv.lib; {
      description = "Supernodal sparse direct solver.";
      license = licenses.bsd3;
      homepage = "https://github.com/xiaoyeli/superlu";
      platform = platforms.unix;
    };
  }
