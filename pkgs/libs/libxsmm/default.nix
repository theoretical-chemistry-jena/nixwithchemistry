# Partially from https://github.com/markuskowa/NixOS-QChem/blob/master/libxsmm/default.nix#L33
{ stdenv, config, gfortran, fetchFromGitHub, utillinux, python3, which
, blas }:

stdenv.mkDerivation rec {
    pname = "libxsmm";
    version = "1.16.1";

    nativeBuildInputs = [
      gfortran
      utillinux
      python3
      which
    ];

    propagatedBuildInputs = [
      blas
    ];

    src = fetchFromGitHub  {
      owner = "hfp";
      repo = pname;
      rev = version;
      sha256= "1c1qj6hcdfx11bvilnly92vgk1niisd2bjw1s8vfyi2f7ws1wnp0";
    };

    postPatch = ''
      for i in ./scripts ./tests .mktmp.sh ./.state.sh; do
        patchShebangs $i
      done
    '';

    dontConfigure = true;

    makeFlags = with stdenv.lib; [
      "STATIC=0"
      "OMP=1"
      "FC=gfortran"
    ]
    ++ lists.optional config.avx2 "AVX=2"
    ;

    hardeningDisable = [
      "format"
    ];

    enableParallelBuilding = true;

    preInstall = ''
      mkdir -p $out
      installFlagsArray+=("PREFIX=$out")
    '';

    meta = with stdenv.lib; {
      description = "Library for specialized dense and sparse matrix operations targeting Intel Architecture";
      homepage = https://github.com/hfp/libxsmm;
      license = licenses.bsd3;
      platforms = [ "x86_64-linux" ] ;
    };
  }
