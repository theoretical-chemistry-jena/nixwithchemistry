{ stdenv, fetchFromGitHub, cmake, gfortran
# Dependencies
, blas
, lapack
}:
assert
  stdenv.lib.asserts.assertMsg
  (!blas.isILP64)
  "32 bit integer BLAS implementation required.";

stdenv.mkDerivation rec {
    pname = "libefp";
    version = "1.5.0";

    nativeBuildInputs = [
      cmake
      gfortran
    ];

    buildInputs = [
      blas
      lapack
    ];

    src = fetchFromGitHub  {
      owner = "ilyak";
      repo = pname;
      rev = version;
      sha256= "1zlihc7hp2vcdcwi82n7xg41bm1vv7n1mx1jdff4w1amhg9rlahm";
    };

    cmakeFlags = [
      "-DENABLE_OPENMP=ON"
      "-DBUILD_SHARED_LIBS=ON"
      "-DFRAGLIB_DEEP=ON"
      "-DFRAGLIB_UNDERSCORE_L=ON"
      "-DINSTALL_DEVEL_HEADERS=ON"
    ];

    hardeningDisable = ["format"];

    meta = with stdenv.lib; {
      description = "Parallel implementation of the Effective Fragment Potential Method";
      homepage = "https://github.com/ilyak/libefp";
      license = licenses.bsd3;
      platforms = platforms.unix;
    };
  }
