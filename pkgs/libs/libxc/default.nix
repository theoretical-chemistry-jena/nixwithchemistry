{ stdenv, gfortran, fetchFromGitLab, fetchpatch, cmake, perl }:
stdenv.mkDerivation rec {
    pname = "libxc";
    version = "5.1.3";

    nativeBuildInputs = [
      cmake
      gfortran
      perl
    ];

    src = fetchFromGitLab  {
      owner = "libxc";
      repo = pname;
      rev = version;
      sha256= "14czspifznsmvvix5hcm1rk18iy590qk8p5m00p0y032gmn9i2zj";
    };

    cmakeFlags = [
      "-DBUILD_SHARED_LIBS=ON"
      "-DCMAKE_BUILD_TYPE=Release"
      "-DBUILD_FPIC=ON"
      "-DENABLE_GENERIC=OFF"
      "-DENABLE_XHOST=OFF"
      "-DENABLE_FORTRAN=ON"
      "-DENABLE_FORTRAN03=ON"
    ];

    meta = with stdenv.lib; {
      description = "Library of exchange-correlation functionals for density-functional theory";
      license = licenses.mpl20;
      homepage = "https://www.tddft.org/programs/libxc/";
      platforms = platforms.unix;
    };
  }
