{ stdenv, fetchurl , gfortran, perl, makeWrapper, symlinkJoin, pkgconfig, config
# Dependencies
, bison
, numactl
, libxml2
, infiniband-diags
, rdma-core
, opensm
, zlib
, libpsm2 ? null
, libfabric ? null
# Runtime dependencies
, openssh
}:
# Check that a valid network has been selected.
assert stdenv.lib.asserts.assertMsg (builtins.elem config.network [ "omnipath" "infiniband" "ethernet" ]) "Unknown network configured";

# Check for support on the system for the network-type.
assert stdenv.lib.asserts.assertMsg (if config.network == "infiniband" then (stdenv.isLinux || stdenv.isFreeBSD) else true) "InfiniBand is only supported on Linux or FreeBSD systems.";
assert stdenv.lib.asserts.assertMsg (if config.network == "omnipath" then (libpsm2 != null && libfabric != null) else true) "Omnipath is only supported if libpsm2 and libfabric support are present.";

stdenv.mkDerivation rec {
    pname = "mvapich2";
    version = "2.3.4";

    nativeBuildInputs = [
      perl
      makeWrapper
      pkgconfig
    ];

    buildInputs = with stdenv; [
      gfortran
      bison
      libxml2
      zlib
    ]
    ++ lib.lists.optional isLinux numactl
    ++ lib.lists.optionals ((isLinux || isFreeBSD) && config.network == "infiniband") [ rdma-core infiniband-diags opensm ]
    ++ lib.lists.optionals (libpsm2 != null && libfabric != null && config.network == "omnipath") [ libpsm2 libfabric ]
    ;

    propagatedBuildInputs = with stdenv; [
      openssh
    ];

    src = fetchurl {
      url = "https://mvapich.cse.ohio-state.edu/download/mvapich/mv2/${pname}-${version}.tar.gz";
      sha256 = "0ypad2g90s8pr8fpnfy162s9jq8qrsf13218bn73qcwqgifa89kj";
    };

    configureFlags = with stdenv; [
      "--with-pm=hydra"
      "--enable-fortran=all"
      "--enable-cxx"
      "--enable-threads=multiple"
      "--enable-hybrid"
      "--enable-shared"
      "--enable-static"
    ]
    ++ lib.lists.optionals (config.network == "ethernet") [ "--with-device=ch3:sock" ]
    ++ lib.lists.optionals ((isLinux || isFreeBSD) && config.network == "infiniband") [ "--with-device=ch3:mrail" "--with-rdma=gen2" ]
    ++ lib.lists.optionals (libpsm2 != null && libfabric != null && config.network == "omnipath") ["--with-device=ch3:psm" "--with-psm2=${libpsm2}"]
    ;

    enableParallelBuilding = true;

    hardeningDisable = [
      "format"
    ];

    # Make a mpiCC executable (symlink), which is expected by some derivations.
    postInstall = ''
      ln $out/bin/mpicxx $out/bin/mpiCC
    '';

    preFixup = ''
      for entry in $out/bin/mpichversion $out/bin/mpivars; do
        patchelf --set-rpath "$out/lib" $entry
      done

      for exe in $out/bin/*; do
        wrapProgram $exe\
          --prefix PATH : "${openssh}/bin" \
          --prefix LD_LIBRARY_PATH : "${openssh}/lib" \
          --set-default MV2_ENABLE_AFFINITY "0"
      done
    '';

    meta = with stdenv.lib; {
      description = "MPI over InfiniBand, Omni-Path, Ethernet/iWARP, and RoCE";
      license = licenses.bsd3;
      homepage = "https://mvapich.cse.ohio-state.edu/";
      platforms = platforms.unix;
    };
  }
