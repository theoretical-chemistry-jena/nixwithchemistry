{ stdenv, fetchFromGitHub, cmake, perl, gfortran, python3
# Dependencies
, boost
, eigen
, zlib
}:
stdenv.mkDerivation rec {
    pname = "pcmsolver";
    version = "1.2.3";

    nativeBuildInputs = [
      cmake
      gfortran
      python3
      perl
    ];

    buildInputs = [
      boost
      eigen
      zlib
    ];

    src = fetchFromGitHub  {
      owner = "PCMSolver";
      repo = pname;
      rev = "v${version}";
      sha256= "117f9ic41xagyzkfwa1sbfcmycc8nlm6ivflkdc228xmp7axvf9m";
    };

    cmakeFlags = [
      "-DENABLE_OPENMP=ON"
    ];

    hardeningDisable = [
      "format"
    ];

    meta = with stdenv.lib; {
      description = "An API for the Polarizable Continuum Model";
      license = licenses.lgpl3;
      homepage = "https://pcmsolver.readthedocs.io/en/stable/";
      platforms = platforms.unix;
    };
  }
