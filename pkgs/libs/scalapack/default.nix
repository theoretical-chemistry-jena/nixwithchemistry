{ stdenv, fetchFromGitHub, gfortran, cmake
# Dependencies
, mpi
, blas
, lapack
}:
assert
  stdenv.lib.asserts.assertMsg
  (!blas.isILP64)
  "32 bit integer BLAS implementation required.";
stdenv.mkDerivation rec {
    pname = "scalapack";
    version = "2.1.0";

    nativeBuildInputs = [
      cmake
      gfortran
    ];

    propagatedBuildInputs = [
      mpi
      blas
      lapack
    ];

    src = fetchFromGitHub  {
      owner = "Reference-ScaLAPACK";
      repo = pname;
      rev = "v${version}";
      sha256= "1c10d18gj3kvpmyv5q246x35hjxaqn4ygy1cygaydhyxnm4klzdj";
    };

    cmakeFlags = [
      "-DBUILD_SHARED_LIBS=ON"
    ];

    hardeningDisable = [
      "format"
    ];

    meta = with stdenv.lib; {
      description = "Library of high-performance linear algebra routines for parallel distributed memory machines";
      license = licenses.bsd3;
      homepage = "http://www.netlib.org/scalapack/";
      platforms = [ "x86_64-linux" ];
    };
  }
