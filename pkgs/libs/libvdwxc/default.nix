{ stdenv, fetchFromGitLab, gfortran, autoreconfHook
, fftw
, mpi
}:
stdenv.mkDerivation rec {
  pname = "libvdwxc";
  # Stable version has non-working MPI detection.
  version = "92f4910c6ac88e111db2fb3a518089d0510c53b0";

  nativeBuildInputs = [
    autoreconfHook
    gfortran
  ];

  buildInputs = [
    fftw
  ];

  propagatedBuildInputs = [
    mpi
  ];

  preConfigure = ''
    mkdir build && cd build

    export PATH=$PATH:${mpi}/bin
    configureFlagsArray+=(
      --with-mpi=${mpi}
      CC=mpicc
      FC=mpif90
      MPICC=mpicc
      MPIFC=mpif90
    )
  '';

  configureScript = "../configure";

  src = fetchFromGitLab  {
    owner = "libvdwxc";
    repo = pname;
    rev = version;
    sha256= "1c7pjrvifncbdyngs2bv185imxbcbq64nka8gshhp8n2ns6fids6";
  };

  hardeningDisable = [
    "format"
  ];

  meta = with stdenv.lib; {
    description = "Portable C library of density functionals with van der Waals interactions for density functional theory";
    license = with licenses; [ lgpl3 bsd3 ];
    homepage = "https://libvdwxc.org/";
    platforms = platforms.unix;
  };
}
