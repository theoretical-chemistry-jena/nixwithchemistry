{ stdenv, gfortran, fetchurl, makeWrapper, rpmextract, which
# Runtime dependencies
, openssh
}:
stdenv.mkDerivation rec {
    pname = "intelmpi";
    version = "2019.7.217";

    nativeBuildInputs = [
      rpmextract
      makeWrapper
      which
    ];

    propagatedBuildInputs = [
      openssh
      gfortran
    ];

    src = fetchurl {
      url = "http://registrationcenter-download.intel.com/akdlm/irc_nas/tec/16546/l_mpi_${version}.tgz";
      sha256 = "90383b0023f84ac003a55d8bb29dbcf0c639f43a25a2d8d8698a16e770ac9c07";
    };

    buildPhase = with stdenv.lib.versions; ''
      rpmextract rpm/intel-mpi-rt-${majorMinor version}-${patch version}-${majorMinor version}-${patch version}.x86_64.rpm
      rpmextract rpm/intel-mpi-sdk-${majorMinor version}-${patch version}-${majorMinor version}-${patch version}.x86_64.rpm
    '';

    /*
    For some reason, the version in the RPMs seems to be a different one than in the tarball
    containing them. Therefore here is a more rough approach to switch the directory.
    */
    installPhase = ''
      # Make sure the output directory in the store exists.
      mkdir -p $out

      # The RPM contains a lot of subdirectories, which all contain only one directory. Step down.
      # For some reason, the version in the RPMs seems to be a different one than in the tarball
      # containing them. Therefore here is a more rough approach to switch the directory.
      cd opt/intel/compilers_and_libraries_*/linux/mpi/intel64

      # Copy the normal directory structure to the store.
      for d in bin etc lib; do
        cp -r $d $out/.
      done
      # The lib directory also contains the most interesting libraries within release_mt. Symlink
      # them to lib directly
      for l in $out/lib/release_mt/*; do
        ln -s $l $out/lib/.
      done

      # Libfabric comes also here as a library, which can (and in this nix packages' case should) be
      # used as the libfabric provider. It resides within its own directory.
      cd libfabric
      for d in bin lib; do
        cp -r $d $out/.
      done
      cd ..

      # Now make a Symlink again to all directories, as intel stuff assumes to have intel64/bin
      # intel64/etc and so on.
      cd $out && ln -s . intel64
    '';

    preFixup = ''
      cd $out/bin
      for exe in $(find $out/bin -type f -executable); do
        wrapProgram $exe \
          --prefix PATH : "${openssh}/bin" \
          --prefix LD_LIBRARY_PATH : "${openssh}/lib" \
          --set FI_PROVIDER_PATH $out/lib/prov \
          --set I_MPI_ROOT $out \
          --set I_MPI_CXX $(which g++) \
          --set I_MPI_CC $(which gcc)
      done
    '';

    # Required by license to not modify the binaries.
    dontStrip = true;
    dontPatchELF = true;

    meta = with stdenv.lib; {
      description = "Intel Message Passing Interface Library";
      license = licenses.unfree;
      homepage = "https://software.intel.com/content/www/us/en/develop/tools/mpi-library.html";
      platforms = platforms.unix;
    };
  }
