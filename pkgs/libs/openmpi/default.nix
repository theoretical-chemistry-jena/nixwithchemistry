{ stdenv, fetchurl , gfortran, perl, makeWrapper, symlinkJoin
# Dependencies
, libnl
, rdma-core
, zlib
, numactl
, libevent
, hwloc
, libpsm2 ? null
, libfabric ? null
, cudatoolkit ? null
# Runtime dependencies
, openssh
}:
let
  cudatoolkit_joined = symlinkJoin {
    name = "${cudatoolkit.name}-unsplit";
    paths = [ cudatoolkit.out cudatoolkit.lib ];
  };
in
stdenv.mkDerivation rec {
    pname = "openmpi";
    version = "4.0.5";

    nativeBuildInputs = [
      perl
      makeWrapper
    ];

    propagatedBuildInputs = with stdenv; [
      gfortran
      zlib
      libevent
      hwloc
      libpsm2
      libfabric
      openssh
    ]
    ++ lib.lists.optionals isLinux [ libnl numactl ]
    ++ lib.lists.optional (cudatoolkit != null) cudatoolkit
    ++ lib.lists.optional (isLinux || isFreeBSD) rdma-core
    ++ lib.lists.optionals (libpsm2 != null && libfabric != null) [ libpsm2 libfabric ]
    ;

    src = with stdenv.lib.versions; fetchurl {
      url = "https://download.open-mpi.org/release/open-mpi/v${majorMinor version}/${pname}-${version}.tar.bz2";
      sha256 = "02f0r9d3xgs08svkmj8v7lzviyxqnkk4yd3z0wql550xnriki3y5";
    };

    postPatch = ''
      # Make sure also /usr/bin/env stuff gets replaced
      patchShebangs ./

      # Make build reproducible by removing host information
      ts=`date -d @$SOURCE_DATE_EPOCH`
      sed -i 's/OPAL_CONFIGURE_USER=.*/OPAL_CONFIGURE_USER="nixbld"/' configure
      sed -i 's/OPAL_CONFIGURE_HOST=.*/OPAL_CONFIGURE_HOST="localhost"/' configure
      sed -i "s/OPAL_CONFIGURE_DATE=.*/OPAL_CONFIGURE_DATE=\"$ts\"/" configure
      find -name "Makefile.in" -exec sed -i "s/\`date\`/$ts/" \{} \;
    '';

    configureFlags = with stdenv; [
      "--enable-mpi-cxx"
      "--enable-shared=yes"
      "--enable-static=yes"
      "--enable-openib-rdmacm=yes"
      "--with-libnl=${libnl.dev}"
      "--with-hwloc=${hwloc.dev}"
      "--with-slurm"
    ]
    ++ lib.lists.optionals (libpsm2 != null && libfabric != null) [ "--with-psm2" "--with-libfabric=${libfabric}" ]
    ++ lib.lists.optionals (cudatoolkit != null) [ "--with-cuda=${cudatoolkit_joined}" "--enable-dlopen" ]
    ;

    enableParallelBuilding = true;
    doCheck = true;

    hardeningDisable = [
      "format"
    ];

    preFixup = ''
      for i in $out/bin/mpiexec $out/bin/mpirun; do
        wrapProgram "$i" \
          --prefix PATH : "${openssh}/bin" \
          --prefix LD_LIBRARY_PATH : "${openssh}/lib"
      done
    '';

    meta = with stdenv.lib; {
      description = "A High Performance Message Passing Library";
      license = licenses.bsd3;
      homepage = "https://www.open-mpi.org/";
      platforms = platforms.unix;
    };
  }
