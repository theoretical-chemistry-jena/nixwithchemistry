{ stdenv, fetchFromGitHub, gfortran, cmake
, blas
, lapack
, bigInt ? false
, mpi ? null
}:
assert
  stdenv.lib.asserts.assertMsg
  (bigInt == blas.isILP64 || blas.passthru.implementation == "mkl")
  "For OpenBLAS a matching integer size is required.";
let
  useMKL64 = bigInt && blas.passthru.implementation == "mkl";
  mkl = blas.passthru.provider;

in
  stdenv.mkDerivation rec {
    pname = "arpack";
    version = "3.7.0";

    nativeBuildInputs = [
      cmake
    ];

    buildInputs = [
      gfortran
      lapack
      (if useMKL64 then mkl else blas)
    ];

    propagatedBuildInputs = stdenv.lib.lists.optional (mpi != null) mpi;

    src = fetchFromGitHub {
      owner = "opencollab";
      repo = "arpack-ng";
      rev = version;
      sha256 = "1x7a1dj3dg43nlpvjlh8jzzbadjyr3mbias6f0256qkmgdyk4izr";
    };

    preConfigure = ''
      cmakeFlagsArray+=(
        -DINTERFACE64=${if bigInt then "ON" else "OFF"}
        -DMPI=${if (mpi != null) then "ON" else "OFF"}

      )
    '';

    hardeningDisable = [
      "format"
    ];

    /*
    Test seem to pass with OpenBLAS but not MKL. When building with MVAPICH2 the sandboxing causes
    one test to fail, as MVAPICH2 cannot determine the number of CPUs.
    */
    doCheck = true;

    meta = with stdenv.lib; {
      description = "Collection of Fortran77 subroutines designed to solve large scale eigenvalue problems. ";
      license = licenses.bsd3;
      homepage = "https://www.caam.rice.edu/software/ARPACK/";
      platform = platforms.unix;
    };
  }
