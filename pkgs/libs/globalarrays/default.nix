{ stdenv, fetchFromGitHub, fetchpatch, gfortran, autoreconfHook
, blas
, lapack
, mpi
, openssh
# Dependencies, depending on configuration.
, libpsm2 ? null
, libfabric ? null
, rdma-core ? null
, elpa
, scalapack
# Configuration
, config
, bigInt ? false # Switches between 32 and 64 bit integers
, forceMPI3 ? false # Force generic communication modell without separate communication process.
}:
assert
  stdenv.lib.asserts.assertMsg
  (bigInt == blas.isILP64 || blas.passthru.implementation == "mkl")
  "For OpenBLAS a matching integer size is required.";

# Look if necessary libraries are present for nework setup.
assert
  stdenv.lib.asserts.assertMsg
  (
    if      config.network == "omnipath" then libpsm2 != null && libfabric != null
      else if config.network == "infiniband" then rdma-core != null
      else true
  )
  "Some dependencies are missing for the selected network type.";

let
  useMKL64 = bigInt && blas.passthru.implementation == "mkl";
  mkl = blas.passthru.provider;
  blasLibs = if useMKL64
    then "-lmkl_intel_ilp64 -lmkl_gnu_thread -lmkl_core -lgomp -lpthread -lm -ldl"
    else "-lblas"
    ;

  scalapack_ = scalapack.override {
    inherit blas lapack mpi;
  };

  elpa_ = elpa.override {
    inherit mpi blas;
    scalapack = scalapack_;
  };

in
  stdenv.mkDerivation rec {
    pname = "globalarrays";
    version = "5.7.2";

    src = fetchFromGitHub {
      owner = "GlobalArrays";
      repo = "ga";
      rev = "v${version}";
      sha256 = "0c1y9a5jpdw9nafzfmvjcln1xc2gklskaly0r1alm18ng9zng33i";
    };

    nativeBuildInputs = [
      autoreconfHook
    ];

    buildInputs = with stdenv.lib; [
      mpi
      (if useMKL64 then mkl else blas)
      gfortran
    ]
    ++ lists.optional (!bigInt) lapack
    ++ lists.optionals (libpsm2 != null && libfabric != null) [ libpsm2 libfabric ]
    ++ lists.optional (rdma-core != null) rdma-core
    ++ lists.optional (!bigInt) scalapack_
    ++ lists.optional (!bigInt) elpa_
    ;

    propagatedBuildInputs = [
      openssh
    ];

    /*
    MPI-PR is the most stable and portable network type. Use this for all setups except infiniband,
    where the native interface is being used.
    Despite the fact that there is native OmniPath support, many test cases fail for --with-ofi and it
    does not work for example on Ara with OmniPath. Therefore also use MPI-PR for OmniPath, which
    performs well.

    Alternatively the network can be forced to MPI3, which does not use a separate communication
    process per node. This is required for example for OpenMOLCAS.
    */
    preConfigure = ''
      configureFlagsArray=( \
        "--enable-static"
        "--enable-shared"
        "--enable-cxx" \
        "--enable-gparrays" \
        "--enable-eispack" \
        "--with-mpi" \
        ${if bigInt then "--enable-i8" else "--enable-i4"} \
        ${if forceMPI3 then "--with-mpi3" else
          if (config.network == "infiniband") then "--with-ofa" else "--with-mpi-pr"} \
        ${if bigInt then "--with-blas8=\"${blasLibs}\"" else "--with-blas4=\"${blasLibs}\" --with-lapack"} \
        ${if (!bigInt) then "--with-scalapack" else ""} \
        ${if (!bigInt) then "--with-elpa" else ""} \
        MPICXX="mpicxx" \
        MPICC="mpicc"
      )
    '';

    enableParallelBuilding = true;

    hardeningDisable = [
      "format"
    ];

    meta = with stdenv.lib; {
      homepage = "hpc.pnl.gov/globalarrays/";
      description = "Partitioned Global Address Space library for distributed arrays ";
      license = licenses.bsd3;
      platforms = platforms.linux;
    };
  }
