{ stdenv, fetchurl, gfortran, perl, libtool
, config
# Dependencies
, xxd
, rdma-core
, scalapack
, cudatoolkit ? null
, mpi ? null
, blas
, openmp ? true
# Python deps.
, python
, numpy
, cython
, mpi4py
}:
assert stdenv.lib.asserts.assertMsg (!blas.isILP64) "32 bit integer BLAS implementation required.";

let
  mpi4py_ = mpi4py.override {
    inherit mpi;
  };

  scalapack_ = scalapack.override {
    inherit blas mpi;
  };

in
  stdenv.mkDerivation rec {
    pname = "elpa";
    version = "2020.05.001";

    nativeBuildInputs = [
      perl
      gfortran
      libtool
    ];

    buildInputs = with stdenv.lib; [
      blas
      xxd
    ]
    ++ lists.optional (cudatoolkit != null) cudatoolkit
    ++ lists.optionals (mpi != null) [ scalapack_ rdma-core ]
    ;

    propagatedBuildInputs = [
      python
      numpy
      cython
    ] ++ stdenv.lib.lists.optionals (mpi != null) [
      mpi
      mpi4py_
    ];

    src = fetchurl  {
      url = "https://elpa.mpcdf.mpg.de/html/Releases/${version}/${pname}-${version}.tar.gz";
      sha256= "1rcw8fb1kc7n9b4j4lx4nfyd4495wxx5idf7bl3q476f6brirzv6";
    };

    preConfigure = ''
      perlScripts=$(find -name "*.pl")
      for f in $perlScripts; do
        patchShebangs $f
      done

      configureFlagsArray+=(
        ${if openmp then "--enable-openmp" else ""}
        "--enable-shared"
        "--enable-static"
        "--enable-python"
        ${if mpi != null then "--with-mpi=yes" else "--with-mpi=no"}
        ${if (!config.sse || !config.sse2 || !config.sse3 || !config.sse4 || !config.sse4_2) then "--disable-sse" "--disable-sse-assembly" else ""}
        ${if (!config.avx) then "--disbale-avx" else ""}
        ${if (!config.avx2 || !config.aes || !config.fma ) then "--disbale-avx2" else ""}
        ${if (!config.avx512 || !config.aes || !config.fma) then "--disable-avx512" else ""}
        ${if cudatoolkit != null then "--enable-gpu" else ""}
        FC=${if mpi != null then "mpif90" else "gfortran"}
        CC=${if mpi != null then "mpicc" else "gcc"}
        CXX=${if mpi != null then "mpicxx" else "g++"}
      )
    '';

    enableParallelBuilding = false;

    hardeningDisable = [
      "format"
    ];

    meta = with stdenv.lib; {
      description = "Eigenvalue Solvers for Petaflop Architectures";
      license = licenses.lgpl2;
      homepage = "https://elpa.mpcdf.mpg.de/";
      platform = platforms.unix;
    };
  }
