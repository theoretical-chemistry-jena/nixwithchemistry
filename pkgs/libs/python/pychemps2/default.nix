{ buildPythonPackage
, hdf5
, hdf5-cpp
, numpy
, cython
, chemps2
}:
buildPythonPackage {
  inherit (chemps2) version meta src;
  pname = "PyCheMPS2";

  buildInputs = [
    chemps2
    hdf5
    hdf5-cpp
  ];

  propagatedBuildInputs = [
    cython
    numpy
  ];

  preConfigure = ''
    cd PyCheMPS2
  '';
}
