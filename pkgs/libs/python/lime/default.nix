{ buildPythonPackage, fetchPypi
, scikitlearn
, scikitimage
, tqdm
}:

buildPythonPackage rec {
  pname = "lime";
  version = "0.2.0.1";

  propagatedBuildInputs = [
    scikitlearn
    scikitimage
    tqdm
  ];

  HOME = "$TMP";
  doCheck = false;

  src = fetchPypi {
    inherit pname version;
    sha256 = "76960e4f055feb53e89b5022383bafc87b63f25bac6265984b0a333d1a57f781";
  };
}
