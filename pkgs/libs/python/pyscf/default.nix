{ stdenv, toPythonModule, cmake, fetchFromGitHub, writeTextFile
, libcint
, libxc
, xcfun
, blas
# Python
, numpy
, scipy
, h5py
, python
}:
assert
  stdenv.lib.asserts.assertMsg
  (!blas.isILP64)
  "A 32 bint integer BLAS implementation is required.";

let
  xcfun_ = xcfun.overrideAttrs (oldAttrs: rec {
    version = "355f42497a9cd17d16ae91da1f1aaaf93756ae8b";
    src = fetchFromGitHub {
      owner = "dftlibs";
      repo = oldAttrs.pname;
      rev = "355f42497a9cd17d16ae91da1f1aaaf93756ae8b";
      sha256 = "09hs8lxks2d98a5q2xky9dz5sfsrxaww3kyryksi9b6l1f1m3hxp";
    };
  });

  pyscf =
    stdenv.mkDerivation rec {
      pname = "pyscf";
      version = "1.7.3";

      src = fetchFromGitHub {
        owner = "pyscf";
        repo = "pyscf";
        rev = "v${version}";
        sha256 = "1gmx75kqyjb8n3jgnlnzamw7f6cibc6wqsfy4vad6crz58lffjjj";
      };

      propagatedBuildInputs = [
        python
        numpy
        scipy
        h5py
      ];

      buildInputs = [
        libcint
        libxc
        xcfun_
        blas
      ];

      nativeBuildInputs = [
        cmake
      ];

      PYSCF_INC_DIR="${libcint}:${libxc}";

      doCheck = false;

      preConfigure = ''
        cd pyscf/lib
      '';

      cmakeFlags = [
        "-DBUILD_LIBCINT=OFF"
        "-DBUILD_LIBXC=OFF"
        "-DBUILD_XCFUN=OFF"
      ];

      installPhase = ''
        cd ../../..

        mkdir -p $out/lib/${python.libPrefix}/site-packages
        cp -r pyscf $out/lib/${python.libPrefix}/site-packages/.

        mkdir -p $out/lib
        for lib in $out/lib/${python.libPrefix}/site-packages/pyscf/lib/*.so ; do
          ln -s $lib $out/lib/.
        done
      '';

      meta = with stdenv.lib; {
        description = "Python-based simulations of chemistry framework";
        homepage = https://pyscf.github.io/;
        license = licenses.asl20;
        platforms = platforms.linux;
      };
    };
in
  toPythonModule pyscf
