{ buildPythonPackage, fetchPypi
, scipy
}:
buildPythonPackage rec {
    pname = "rmsd";
    version = "1.3.2";

    propagatedBuildInputs = [
      scipy 
    ];

    src = fetchPypi  {
      inherit pname version;
      sha256 = "21c7f16a3f90e036663456f52585e83791eb8566cf1111302c1a8c3465328367";
    };
  }
