{ stdenv, fetchurl, gfortran, pkgconfig, cmake
, hdf5-cpp
, blas
, lapack
, arpack
, superlu
}:
/*
assert
  stdenv.lib.asserts.assertMsg
  (!blas.isILP64)
  "A 32 bit integer implementation of BLAS is required.";
*/
let
  arpack_ = arpack.override {
    inherit blas lapack;
    bigInt = blas.isILP64;
  };

  superlu_ = superlu.override {
    inherit blas;
  };

in
  stdenv.mkDerivation rec {
    pname = "armadillo";
    version = "9.900.3";

    nativeBuildInputs = [
      gfortran
      pkgconfig
      cmake
    ];

    buildInputs = with stdenv.lib; [
      blas
      lapack
      hdf5-cpp
      arpack_
      superlu_
    ];

    src = fetchurl  {
      url = "http://sourceforge.net/projects/arma/files/${pname}-${version}.tar.xz";
      sha256= "02pwhf3y2qq50dswjvfcijaw938d1zi1hxr17msv2x1ahlvff5fn";
    };

    cmakeFlags = [
      "-DBUILD_SHARED_LIBS=ON"
      "-DARPACK_LIBRARY=${arpack_}/lib"
    ];

    hardeningDisable = [
      "format"
    ];

    meta = with stdenv.lib; {
      description = "Eigenvalue Solvers for Petaflop Architectures";
      license = licenses.lgpl2;
      homepage = "https://elpa.mpcdf.mpg.de/";
      platform = platforms.unix;
    };
  }
